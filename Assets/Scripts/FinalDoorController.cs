using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinalDoorController : MonoBehaviour
{
    [SerializeField] int AltarsCompleted;

    [SerializeField] Animator anim;


    public void TryOpen()
    {
        AltarsCompleted++;
        if (AltarsCompleted >= 6)
        {
            OpenDoor();
        }
    }

    public void OpenDoor() 
    {
        anim.Play("OpenDoor");
    }
}
