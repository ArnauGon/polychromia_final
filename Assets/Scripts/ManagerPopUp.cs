using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ManagerPopUp : MonoBehaviour
{
    Pool poolPopUp;

    private void Awake()
    {
        poolPopUp = GetComponent<Pool>();
    }

    public void PopUpDMG(int dmg, int b, int c, Vector2 d)
    {
        GameObject clone = poolPopUp.getElement();
        clone.transform.position = new Vector2(d.x, d.y);
        clone.GetComponentInChildren<TMPro.TextMeshPro>().text = dmg+"";

        poolPopUp.ReturnElement(clone, 1f);
    }
}
