using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerScript : MonoBehaviour
{

    [SerializeField] float jumpForce;
    [SerializeField] float bolsa_fuerza;
    [SerializeField] Animator animator;

    [SerializeField] ScriptablePinturas[] scrPinturas;
    [SerializeField] ScriptableProta scrProta;
    [SerializeField] ScriptableCooldown bolsaCooldown;
    [SerializeField] ScriptableCooldown rodaCooldown;
    [SerializeField] ScriptableCooldown grabCooldown;
    [SerializeField] ScriptableCooldown paintWeaponCooldown;

    [SerializeField] Material GlowPlayer;


    Rigidbody2D rb;
    LineRenderer lr;
    Vector2 DragStartPos;

    [SerializeField] GameObject hitboxPlayer;

    [SerializeField] LayerMask groundLayer;
    [SerializeField] LayerMask fuenteVida;
    [SerializeField] LayerMask fuentePintura;
    [SerializeField] LayerMask fuenteSave;

    [SerializeField] GameEvent loadGame;
    [SerializeField] GameEvent respawn;

    [SerializeField] GameObject bolsa;
    [SerializeField] GameObject redProjectile;
    [SerializeField] GameObject yellowProjectile;


    [SerializeField] GameEventInteger eventCanviColHud;
    [SerializeField] GameEventInteger changeColorSword;

    [SerializeField] GameEvent eventActiveDeathCam;
    [SerializeField] GameEvent eventCDBolsa;
    [SerializeField] GameEvent eventModifyHP;
    [SerializeField] GameEvent eventModifyPaint;
    [SerializeField] GameEvent eventGuanyarEXP;

    [SerializeField] AudioSource audio_source;
    [SerializeField] AudioClip[] sonidos;

    [SerializeField] BoxCollider2D boxCollider2d;
    AtacarManager atkManager;

    int contadorBolsas = 0;
    int atkOriginal = 10;
    bool onCanAttack = true;
    bool cdGrab = true;
    bool isTouchingWall;
    bool isTouchingWallAdalt;
    bool isTouchingWallAbaix;
    bool dondeMira = true;
    bool cdRueda = true;
    bool isAttacking = false;
    bool onCanMove = true;
    bool inYellowPaint = false;
    bool grounded = true;
    bool swordNoPaint;
    bool swordRedPaint;
    bool swordBluePaint;
    bool swordYellowPaint;

    Vector2 worldPoint;

    float gravetat;
    [SerializeField] Estat statePlayer;

    private IEnumerator pintarArmesReset;

    public int ContadorBolsas { get => contadorBolsas; set => contadorBolsas = value; }
    public bool OnCanMove { get => onCanMove; set => onCanMove = value; }

    Color colorRed = new Color(255, 0, 0, 100);
    Color colorYellow = new Color(255, 255, 0, 100);
    Color colorBlue = new Color(0, 0, 255, 100);
    Color colorNormal = new Color(0, 0, 0, 100);


    private void Awake()
    {
        AwakeFunction();
    }

    public void AwakeFunction() 
    {
        scrProta.spd = scrProta.spdOriginal;
        atkManager = this.GetComponent<AtacarManager>();
        boxCollider2d = transform.GetComponent<BoxCollider2D>();
        audio_source = this.GetComponent<AudioSource>();
        lr = GetComponent<LineRenderer>();
        rb = this.GetComponent<Rigidbody2D>();
        animator = this.GetComponent<Animator>();
        audio_source = GetComponent<AudioSource>();
        gravetat = rb.gravityScale;
        scrProta.weaponColor = WeaponColor.None;
        this.gameObject.GetComponent<PlayerScript>().enabled = true;

        LoadPosition();
    }

    private void Start()
    {
        SetSkillAvailable(bolsaCooldown);
        EnterState(Estat.ONGROUNDED);
     
        GlowPlayer.SetColor("_Color", colorNormal);
        pintarArmesReset = pintarArmes();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {

            if (pintarArmesReset != null)
            {
                StopCoroutine(pintarArmesReset);

                pintarArmesReset = pintarArmes();
                StartCoroutine(pintarArmesReset);
            }
            else
            {
               StartCoroutine(pintarArmesReset);
            }
        }

        worldPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        TirarBolsa();
        canviarPinturas();
        TocarFuente();
        grounded = IsGrounded();
        PlayerState();
    }

    private void FixedUpdate()
    {
        Move();
        InformacioVoltant();

        if(statePlayer == Estat.ONGROUNDED)
        {
            if (Mathf.Abs(rb.velocity.x) >= 0.1f)
            {
                animator.Play("Run");
            }
            else if(rb.velocity.x == 0)
            {
                animator.Play("Idle1");
            }
        }
    }


    //--------------------------MAQUINA D'ESTAT------------------------------
    public enum Estat
    {
        ONGROUNDED,
        ONAIR,
        ONSLIDE,
        ONGRAB,
        ONROLL,
        ONPADORU,
        KNOCKBACK,
        ONWALLSLOW,
        ONDEAD,

        ONGROUNDED8H,
        ONAIR2H,
        ONAIR6H,
        ONAIR8H,
        COMBO1,
        COMBO11,
        COMBO111,
        COMBOAD11,
        COMBOAD111
    }

    public void ChangeState(Estat nouEstat)
    {
        ExitState(statePlayer);
        EnterState(nouEstat);
    }

    public void EnterState(Estat nouEstat)
    {
        statePlayer = nouEstat;

        if (nouEstat == Estat.ONROLL)
        {
            animator.Play("Roll");
            audio_source.PlayOneShot(sonidos[5], 0.25f);
        }

        if (nouEstat == Estat.ONGROUNDED)
        {
            animator.Play("Idle1");
        }
        
        if (nouEstat == Estat.ONAIR && rb.velocity.y > 0)
        {
            scrProta.atk = 10;
            animator.Play("Jump");
        }
        else if (nouEstat == Estat.ONAIR && rb.velocity.y < 0)
        {
            animator.Play("Fall");
        }
        if (nouEstat == Estat.ONSLIDE)
        {
            animator.Play("WallSlide");
        }
        if (nouEstat == Estat.ONGRAB)
        {
            animator.Play("WallGrab");
            WallGrab();
        }

        if (nouEstat == Estat.KNOCKBACK)
        {
            OnCanMove = false;
            this.gameObject.layer = 17;
            animator.Play("Knockback");
            StartCoroutine(OnHittedInmortal());

        }
        if (nouEstat == Estat.ONWALLSLOW)
        {
            inYellowPaint = true;
            rb.velocity = new Vector2(0, 0);
            rb.gravityScale = 0;
            animator.Play("WallSlide");
        }
        if (nouEstat == Estat.COMBO111)
        {
            changeTagWeaponColor();
        }
        if (nouEstat == Estat.COMBOAD111)
        {
            changeTagWeaponColor();
        }
        if (nouEstat == Estat.ONDEAD)
        {
            audio_source.PlayOneShot(sonidos[9], 0.15f);
            this.gameObject.layer = 25;
            animator.Play("Death");
            this.gameObject.GetComponent<PlayerScript>().enabled = false;
        }
        if (nouEstat == Estat.ONAIR2H)
        {
            audio_source.PlayOneShot(sonidos[1], 1f);
        }
        if (nouEstat == Estat.ONAIR6H)
        {
            audio_source.PlayOneShot(sonidos[2], 1f);
        }
        if (nouEstat == Estat.ONAIR8H)
        {
            audio_source.PlayOneShot(sonidos[1], 1f);
        }
        if (nouEstat == Estat.ONGROUNDED8H)
        {
            audio_source.PlayOneShot(sonidos[2], 1f);
        }
    }

    public void ExitState(Estat estatActual)
    {
        if (estatActual == Estat.ONROLL)
        {
            scrProta.spd = scrProta.spdOriginal;
        }
        if (estatActual == Estat.ONGRAB)
        {
            rb.gravityScale = gravetat;
        }
        if (estatActual == Estat.KNOCKBACK)
        {
            this.gameObject.layer = 6;
        }
        if (estatActual == Estat.COMBO111)
        {
            hitboxPlayer.transform.tag = "PlayerHitbox";
        }
        if (estatActual == Estat.COMBOAD111)
        {
            hitboxPlayer.transform.tag = "PlayerHitbox";
        }
        if (estatActual == Estat.ONDEAD)
        {
            this.gameObject.GetComponent<PlayerScript>().enabled = true;
            this.gameObject.layer = 6;
        }

    }

    void PlayerState()
    {
        switch (statePlayer)
        {
            case Estat.ONGROUNDED:

                if (!grounded)
                {
                    ChangeState(Estat.ONAIR);
                }
          
                if (Input.GetMouseButtonDown(0))
                {
                    ChangeState(Estat.COMBO1);
                }

                if (Input.GetMouseButtonDown(0) && Input.GetKey(KeyCode.W))
                {
                    ChangeState(Estat.ONGROUNDED8H);
                }

                if(Input.GetKey(KeyCode.P) && Input.GetKey(KeyCode.A))
                {
                    ChangeState(Estat.ONPADORU);
                }

                Jump();

                if (Input.GetKeyDown(KeyCode.LeftControl) && !IsOnCooldown(rodaCooldown))
                {
                    StartCoroutine(Rueda());
                }
                break;


            case Estat.ONPADORU:
              
                animator.Play("Padoru");
                break;

            case Estat.ONAIR:
         
                if (!isTouchingWall && rb.velocity.y > 0 && !grounded)
                {
                    animator.Play("Jump");
                }
                else if (!isTouchingWall && rb.velocity.y <= 0 && !grounded)
                {
                    animator.Play("Fall");
                }

                if (isTouchingWall && rb.velocity.y < 0 && !grounded)
                {
                    ChangeState(Estat.ONSLIDE);
                }


                if (grounded)
                {
                    ChangeState(Estat.ONGROUNDED);
                }

                if (isTouchingWall && !isTouchingWallAdalt && !grounded && cdGrab)
                {
                    ChangeState(Estat.ONGRAB);
                }


                if (Input.GetMouseButtonDown(0) && Input.GetKey(KeyCode.S)) 
                {
                    ChangeState(Estat.ONAIR2H);
                }
                else if (Input.GetMouseButtonDown(0) && Input.GetKey(KeyCode.W))
                {
                    ChangeState(Estat.ONAIR8H);
                }
                else if (Input.GetMouseButtonDown(0))
                {
                    ChangeState(Estat.ONAIR6H);
                }

                break;

            case Estat.ONSLIDE:
                
                WallSlide();
                Jump();

                if (!isTouchingWall && rb.velocity.y < 0 && !grounded)
                {
                    ChangeState(Estat.ONAIR);
                }
                if (!isTouchingWall && rb.velocity.y > 0 && !grounded)
                {
                    ChangeState(Estat.ONAIR);
                }
                if (grounded)
                {
                    ChangeState(Estat.ONGROUNDED);
                }

                break;

            case Estat.ONGRAB:

                if (Input.GetKeyDown(KeyCode.Space))
                {
                    StartCoroutine(JumpOnGrab());
                }

                if (!isTouchingWall && !grounded || Input.GetKeyDown(KeyCode.S))
                {
                    ChangeState(Estat.ONAIR);
                }

                break;

            case Estat.ONROLL:
                Jump();
                break;

            case Estat.KNOCKBACK:
                if (Input.GetKeyDown(KeyCode.Space))
                {
                    OnCanMove = true;
                }
                Jump();
                break;
            case Estat.ONWALLSLOW:
                Jump();
                break;

            case Estat.ONDEAD:
                
                break;


            case Estat.COMBO1:
                animator.Play("Attack1");

                if (Input.GetMouseButtonDown(0) && onCanAttack)
                {
                    ChangeState(Estat.COMBO11);
                }

                if (Input.GetMouseButtonDown(0) && onCanAttack && (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.A)))
                {
                    ChangeState(Estat.COMBOAD11);
                }
                break;

            case Estat.COMBO11:
                animator.Play("Attack2");

                if (Input.GetMouseButtonDown(0) && onCanAttack)
                {
                    onCanAttack = false;
                    ChangeState(Estat.COMBO111);
                }
                break;

            case Estat.COMBO111:
                animator.Play("Attack3");
                break;

            case Estat.COMBOAD11:
                animator.Play("Attack3");

                if (Input.GetMouseButtonDown(0) && onCanAttack && (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.A)))
                {
                    ChangeState(Estat.COMBOAD111);
                }
                break;

            case Estat.COMBOAD111:
                animator.Play("Attack1");
                break;

            case Estat.ONGROUNDED8H:
                animator.Play("AttackGrounded8H");
                break;

            case Estat.ONAIR2H:
                animator.Play("AttackJump");
                break;

            case Estat.ONAIR6H:
                animator.Play("AttackJump6H");
                break;

            case Estat.ONAIR8H:
                animator.Play("AttackJump8H");
                break;
            default:
                break;
        }
    }

 
    //---------------------------COMBOS-------------------------------------
    public void canAttack()
    {
        onCanAttack = true;
    }


    public void ChangeAtk(int attack)
    {
        scrProta.atk = scrProta.atkOriginal;

        int atk1 = scrProta.atk;
        int atk2 = scrProta.atk + attack;

        int atkFinal = Random.Range(atk1, atk2);

        this.scrProta.atk = atkFinal;
    }

    public void GainExp(int exp_gain)
    {
        scrProta.exp += exp_gain;

        if (scrProta.exp >= 100)
        {
            this.scrProta.lvl++;
            this.scrProta.max_hp += Random.Range(1, 3);
            this.scrProta.pintura_max += Random.Range(1, 3);
            this.scrProta.atkOriginal += 1;
            scrProta.exp = 0;
        }

        eventGuanyarEXP.Raise();
        eventModifyPaint.Raise();
        eventModifyHP.Raise();
    }



    public IEnumerator OnHittedInmortal()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            OnCanMove = true;
        }
        yield return new WaitForSeconds(1.2f);
        OnCanMove = true;
        ChangeState(Estat.ONAIR);
    }


   


    //---------------------------INFORMACIO--------------------------------------

    bool IsGrounded()
    {
        Collider2D hit2 = Physics2D.OverlapCircle(transform.position - Vector3.up * boxCollider2d.bounds.extents.y, boxCollider2d.bounds.extents.x / 4 * transform.localScale.x, groundLayer);

        if (hit2 != null)
        {
            return true;
        }
        return false;
    }


    public void InformacioVoltant()
    {
        isTouchingWall = Physics2D.Raycast(new Vector2(this.transform.position.x, this.transform.position.y + 0.2f), transform.right, 0.65f, groundLayer);
        isTouchingWallAdalt = Physics2D.Raycast(new Vector2(this.transform.position.x, this.transform.position.y + 0.3f), transform.right, 0.65f, groundLayer);
        isTouchingWallAbaix = Physics2D.Raycast(new Vector2(this.transform.position.x, this.transform.position.y - 0.75f), transform.right, 0.65f, groundLayer);

        Debug.DrawRay(new Vector2(this.transform.position.x, this.transform.position.y + 0.2f), transform.right * 0.65f, Color.green);
        Debug.DrawRay(new Vector2(this.transform.position.x, this.transform.position.y + 0.3f), transform.right * 0.65f, Color.cyan);
        Debug.DrawRay(new Vector2(this.transform.position.x, this.transform.position.y - 0.75f), transform.right * 0.65f, Color.red);


    }



    //-------------------EVENTOS RECIVIR Y GANAR COSAS--------------------

    public void RecieveDmg(int n, GameObject posDmg)
    {
        scrProta.hp -= n;
        eventModifyHP.Raise();

        if (scrProta.hp < 0)
        {
            scrProta.hp = 0;
            ChangeState(Estat.ONDEAD);
        }
        else 
        {

            if (transform.position.x < posDmg.transform.position.x)
            {
                rb.velocity = new Vector2(-4, 5);
                ChangeState(Estat.KNOCKBACK);
            }
            else if (transform.position.x > posDmg.transform.position.x)
            {
                rb.velocity = new Vector2(4, 5);
                ChangeState(Estat.KNOCKBACK);
            }
        }
    }

    public void GuanyarVida(int n)
    {
        scrProta.hp += n;

        if (scrProta.hp > scrProta.max_hp)
        {
            scrProta.hp = scrProta.max_hp;
        }

        eventModifyHP.Raise();
    }

    public void GuanyarPintura(int n)
    {
        scrProta.pintura += n;

        if (scrProta.pintura > scrProta.pintura_max)
        {
            scrProta.pintura = scrProta.pintura_max;
        }
   
        eventModifyPaint.Raise();
    }

    void TocarFuente()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            RaycastHit2D cercleFuenteVida = Physics2D.CircleCast(transform.position, 2, Vector2.zero, 2, fuenteVida);
            RaycastHit2D cercleFuentePintura = Physics2D.CircleCast(transform.position, 2, Vector2.zero, 2, fuentePintura);
            RaycastHit2D cercleSave = Physics2D.CircleCast(transform.position, 2, Vector2.zero, 2, fuenteSave);

            if (cercleFuenteVida.collider != null)
            {
                cercleFuenteVida.collider.GetComponent<FuentesManager>().comportamentFont();
                audio_source.PlayOneShot(sonidos[7], 0.25f);
            }
            else if (cercleFuentePintura.collider != null)
            {
                cercleFuentePintura.collider.GetComponent<FuentesManager>().comportamentFont();
                audio_source.PlayOneShot(sonidos[7], 0.25f);
            }
            else if (cercleSave.collider != null)
            {
                scrProta.position = this.transform.position;
                scrProta.actualScene = SceneManager.GetActiveScene().name;
                cercleSave.collider.GetComponent<FuenteSave>().FuenteSaveGame();
                audio_source.PlayOneShot(sonidos[8], 0.25f);
            }
        }
    }

    //---------------------------PINTURAS--------------------------------------
    public void canviarPinturas()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            ContadorBolsas = 0;
            eventCanviColHud.Raise(0, 0,0, new Vector2(0, 0));
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            ContadorBolsas = 1;
            eventCanviColHud.Raise(1, 1, 1, new Vector2(0, 0));
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            ContadorBolsas = 2;
            eventCanviColHud.Raise(2, 2, 2, new Vector2(0,0));
        }
    }


    public IEnumerator pintarArmes()
    {
        if (scrProta.pintura > 0)
        {
            if (contadorBolsas == 0)
            {
                this.GetComponent<SpriteRenderer>().color = new Color(scrPinturas[contadorBolsas].color.r, scrPinturas[contadorBolsas].color.g, scrPinturas[contadorBolsas].color.b, 100); swordNoPaint = false;
                scrProta.weaponColor = WeaponColor.Red;
                changeColorSword.Raise(1, 0, 0, new Vector2(0, 0));
                GlowPlayer.SetColor("_Color", colorRed);
                scrProta.pintura -= 20;
            }
            else if (contadorBolsas == 1)
            {
                this.GetComponent<SpriteRenderer>().color = new Color(scrPinturas[contadorBolsas].color.r, scrPinturas[contadorBolsas].color.g, scrPinturas[contadorBolsas].color.b, 100); swordNoPaint = false;
                scrProta.weaponColor = WeaponColor.Blue;
                changeColorSword.Raise(2, 0, 0, new Vector2(0, 0));
                GlowPlayer.SetColor("_Color", colorBlue);
                scrProta.pintura -= 20;
            }
            else if (contadorBolsas == 2)
            {
                this.GetComponent<SpriteRenderer>().color = new Color(scrPinturas[contadorBolsas].color.r, scrPinturas[contadorBolsas].color.g, scrPinturas[contadorBolsas].color.b, 100); swordNoPaint = false;
                scrProta.weaponColor = WeaponColor.Yellow;
                changeColorSword.Raise(3, 0, 0, new Vector2(0, 0));
                GlowPlayer.SetColor("_Color", colorYellow);
                scrProta.pintura -= 20;
            }
            if (scrProta.pintura < 0)
            {
                scrProta.pintura = 0;
            }
            eventModifyPaint.Raise();

            yield return new WaitForSeconds(7f);
            GlowPlayer.SetColor("_Color", colorNormal);
            scrProta.weaponColor = WeaponColor.None;
            changeColorSword.Raise(0, 0, 0, new Vector2(0, 0));
        }

    }

    private void changeTagWeaponColor()
    {
        //RED
        if (scrProta.weaponColor == WeaponColor.Red)
        {
            audio_source.PlayOneShot(sonidos[4], 0.2f);
            hitboxPlayer.transform.tag = "WeaponRed";
            GameObject projectileRed = Instantiate(redProjectile, new Vector3(this.transform.position.x, this.transform.position.y, 0), Quaternion.identity);

            if (dondeMira)
            {
                projectileRed.transform.rotation = Quaternion.Euler(0, 0, 90);
                projectileRed.GetComponent<Rigidbody2D>().velocity = new Vector2(8, 0);
            }
            else if (!dondeMira)
            {
                projectileRed.GetComponent<Rigidbody2D>().velocity = new Vector2(-8, 0);
                projectileRed.transform.rotation = Quaternion.Euler(0, 0, -90);
            }
        }
        //BLUE
        else if (scrProta.weaponColor == WeaponColor.Blue)
        {
            hitboxPlayer.transform.tag = "WeaponBlue";
        }
        //YELLOW
        else if (scrProta.weaponColor == WeaponColor.Yellow)
        {
            audio_source.PlayOneShot(sonidos[4], 0.2f);
            hitboxPlayer.transform.tag = "WeaponYellow";
            if (dondeMira)
            {
                GameObject projectileYellow = Instantiate(yellowProjectile, new Vector3(this.transform.position.x + 1, this.transform.position.y, 0), Quaternion.identity);
                projectileYellow.transform.rotation = Quaternion.Euler(0, 0, 0);
                projectileYellow.GetComponent<Rigidbody2D>().velocity = new Vector2(15, 0);
                Destroy(projectileYellow, 1f);
            }
            else if (!dondeMira)
            {
                GameObject projectileYellow = Instantiate(yellowProjectile, new Vector3(this.transform.position.x - 1, this.transform.position.y, 0), Quaternion.identity);
                projectileYellow.transform.rotation = Quaternion.Euler(0, 180, 0);
                projectileYellow.GetComponent<Rigidbody2D>().velocity = new Vector2(-15, 0);
                Destroy(projectileYellow, 1f);
            }
        }
    }



    public void GreenFloor(float duration)
    {
        StartCoroutine(GreenFloorTimer(duration));
    }

    IEnumerator GreenFloorTimer(float duration)
    {
        this.gameObject.transform.localScale = new Vector3(this.transform.localScale.x / 2, this.transform.localScale.y / 2, this.transform.localScale.z);

        yield return new WaitForSeconds(duration);
        this.gameObject.transform.localScale = new Vector3(1, 1, this.transform.localScale.z);
    }

    public void TirarBolsa()
    {
        if (Input.GetMouseButtonDown(1) && !IsOnCooldown(bolsaCooldown) && scrProta.pintura >= 10)
        {
            StartCoroutine(SkillCooldown(bolsaCooldown));
            eventCDBolsa.Raise();

            GameObject bolsaClone = Instantiate(bolsa);
            bolsaClone.transform.position = this.transform.position;
            Vector2 DragEndPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector2 PositionV2 = new Vector2(bolsaClone.transform.position.x, bolsaClone.transform.position.y);
            Vector2 _velocity = (DragEndPos - PositionV2) * bolsa_fuerza;
            bolsaClone.GetComponent<Rigidbody2D>().velocity = _velocity;
            bolsaClone.GetComponent<BolsaScript>().scriptablePintura = scrPinturas[ContadorBolsas];
            Color colso = bolsaClone.GetComponent<BolsaScript>().scriptablePintura.color;
            bolsaClone.GetComponent<BolsaScript>().Colorpelota = new Color(colso.r, colso.g, colso.b);
            bolsaClone.GetComponent<Rigidbody2D>().AddTorque(10f);
            scrProta.pintura -= 10;

            if(scrProta.pintura < 0)
            {
                scrProta.pintura = 0;
            }
            eventModifyPaint.Raise();
        }
    }

    public void Slow(float n)
    {
        scrProta.spd = n;


        if(statePlayer == Estat.ONSLIDE || statePlayer == Estat.ONAIR)
        {
            ChangeState(Estat.ONWALLSLOW);
        }
    }


    public void SlowReset()
    {
        scrProta.spd = scrProta.spdOriginal;
        inYellowPaint = false;
        rb.gravityScale = gravetat;
        if (isTouchingWall && !grounded)
        {
            ChangeState(Estat.ONSLIDE);
        }
        else if (!grounded)
        {
            ChangeState(Estat.ONAIR);
        } 
    }

    //---------------------------COOLDOWNS--------------------------------------

    private void SetSkillAvailable(ScriptableCooldown skill)
    {
        skill.elapsedTime = skill.cooldown;
    }

    private bool IsOnCooldown(ScriptableCooldown skill)
    {
        return skill.elapsedTime < skill.cooldown;
    }

    private IEnumerator SkillCooldown(ScriptableCooldown skill)
    {
        float frequency = 0.1f;
        skill.elapsedTime = 0;

        while (skill.elapsedTime <= skill.cooldown)
        {
            yield return new WaitForSeconds(frequency);
            skill.elapsedTime += frequency;
        }
        skill.elapsedTime = skill.cooldown;
    }


    //---------------------------MOVIMENT--------------------------------------


    public void Move()
    {

        if (Input.GetKey("t"))
        {
            animator.Play("Padoru");
        }

        if (OnCanMove) 
        {
            if (Input.GetKey("d"))
            {
                dondeMira = true;
                transform.localRotation = Quaternion.Euler(0, 0, 0);
                rb.velocity = new Vector2(scrProta.spd, rb.velocity.y);
            }
            else if (Input.GetKey("a"))
            {
                dondeMira = false;
                transform.localRotation = Quaternion.Euler(0, 180, 0);
                rb.velocity = new Vector2(-scrProta.spd, rb.velocity.y);
            }
            else
            {
                //rb.velocity = new Vector2(0, rb.velocity.y);
            }
        }
    }


    public void Jump()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (grounded || statePlayer == Estat.ONWALLSLOW)
            {
                audio_source.PlayOneShot(sonidos[3], 0.25f);
                this.gameObject.layer = 6;
                rb.velocity = new Vector2(this.rb.velocity.x, jumpForce * transform.localScale.x);
            }
        }
    }

    public IEnumerator JumpOnGrab()
    {
        cdGrab = false;
        audio_source.PlayOneShot(sonidos[3], 0.25f);
        rb.velocity = new Vector2(this.rb.velocity.x, jumpForce * transform.localScale.x);
        yield return new WaitForSeconds(1f);

        cdGrab = true;
    }

    public IEnumerator Rueda()
    {
            ChangeState(Estat.ONROLL);
            StartCoroutine(SkillCooldown(rodaCooldown));
            animator.Play("Roll");

            this.gameObject.layer = 17;
            scrProta.spd += 5;


            if (dondeMira)
            {
                cdRueda = false;
                yield return new WaitForSeconds(0.7f);
                this.gameObject.layer = 6;

                cdRueda = true;
            }
            else if (!dondeMira)
            {
                cdRueda = false;
                yield return new WaitForSeconds(0.7f);
                this.gameObject.layer = 6;

                cdRueda = true;
            }
            ChangeState(Estat.ONGROUNDED);
    }


    public void WallGrab()
    {
        if (isTouchingWall && !isTouchingWallAdalt && !grounded)
        {
            rb.velocity = new Vector2(0, 0);
            rb.gravityScale = 0;
        }
    }

    public void WallSlide()
    {
        if (isTouchingWall && rb.velocity.y < 0 && !grounded && !inYellowPaint)
        {
            rb.velocity = new Vector2(rb.velocity.x, Mathf.Clamp(rb.velocity.y, -5, -4));
        }
    }


  

    //-----------------------Altres-----------------------------

    private void ActivateDeathCamVoid()
    {
        eventActiveDeathCam.Raise();
    }

    public void pararTemps()
    {
        Time.timeScale = 0;
    }

    public void AudioSelection(int index)
    {
        audio_source.PlayOneShot(sonidos[index]);
    }

    public void OnDeath()
    {
        eventActiveDeathCam.Raise();
    }

    public void LoadPosition()
    {

        if (SceneManager.GetActiveScene().name == "Tutorial")
        {
            if (scrProta.hasBeenTuto)
            {
                this.transform.position = scrProta.position;
            }
            else
            {
                //scrProta.hasBeenTuto = true;
            }
        }

        if (SceneManager.GetActiveScene().name == "Nivell1")
        {
            if (scrProta.hasBeenNivel1)
            {
                this.transform.position = scrProta.position;
            }
            else
            {
                //scrProta.hasBeenNivel1 = true;
            }
        }

    }

    //----------------------------COLLISION--------------------------------------


    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.transform.tag == "Exp")
        {
            audio_source.PlayOneShot(sonidos[6], 0.20f);
            GainExp(Random.Range(1,10));
            Destroy(collision.gameObject);
        }
        if (collision.transform.tag == "ObjectHP")
        {
            audio_source.PlayOneShot(sonidos[6], 0.20f);
            GuanyarVida(10);
            Destroy(collision.gameObject);
        }
        if (collision.transform.tag == "ObjectPintura")
        {
            audio_source.PlayOneShot(sonidos[6], 0.20f);
            GuanyarPintura(10);
            Destroy(collision.gameObject);
        }
        if (collision.gameObject.tag == "EnemyHurtbox")
        {
            this.RecieveDmg(5, collision.gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "RedBall" || collision.gameObject.tag == "Spikes") 
        {
            respawn.Raise();
        }
    }

}