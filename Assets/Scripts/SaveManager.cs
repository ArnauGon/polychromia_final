using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using OdinSerializer;

public class SaveManager : MonoBehaviour
{
    [SerializeField] GameObject Player;
    [SerializeField] ScriptableProta m_scrProta;
    [SerializeField] GameEvent modifyHP;
    [SerializeField] GameEvent modifyPaint;
    [SerializeField] GameEvent modifyEXP;

    [SerializeField] GameObject[] maps;
    LevelManager levelManager;

    public static SaveManager saveManagerInstance { get; private set; }


    private void Awake()
    {
        if (saveManagerInstance != null)
        {
            Destroy(this.gameObject);
        }
        else { 
        saveManagerInstance = this;
        
        }

        DontDestroyOnLoad(this.gameObject);
        SceneManager.sceneLoaded += onSceneLoaded;
    }

    private void onSceneLoaded(Scene scene, LoadSceneMode loadSceneMode)
    {
        GameObject go_levelManager = GameObject.Find("LevelManagerNivell1");

        if (go_levelManager != null) 
        {
            levelManager = go_levelManager.GetComponent<LevelManager>();
        }
        if (scene.name == "Tutorial" || scene.name == "Nivell1") 
        {
            Player = GameObject.FindGameObjectWithTag("PlayerHurtbox");
            Player.GetComponent<PlayerScript>().AwakeFunction();

            modifyHP.Raise();
            modifyPaint.Raise();
            modifyEXP.Raise();

            if (levelManager != null)
            {
                levelManager.SetMaps(m_scrProta);
            }
            Player.GetComponent<PlayerScript>().LoadPosition();
            Player.transform.localScale = new Vector3(1, 1, this.transform.localScale.z);
            Player.GetComponent<PlayerScript>().ChangeState(PlayerScript.Estat.ONAIR);
        }
        if (levelManager != null)
        {
            levelManager.SetMaps(m_scrProta);
        }

        
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            LoadGame();
        }
    }

    public void SaveGame()
    { 
        if (SceneManager.GetActiveScene().name == "Tutorial")
        {
            m_scrProta.hasBeenTuto = true;
        }

        if (SceneManager.GetActiveScene().name == "Nivell1")
        {
            m_scrProta.hasBeenNivel1 = true;
        }
        string jsonStr = JsonUtility.ToJson(m_scrProta);
        File.WriteAllText("save.json",jsonStr);
    }

  
    public void LoadGame()
    {       //LLegeix el JSON i sobreescriu e ScriptableProta
            string jsonStr = File.ReadAllText("save.json");
            JsonUtility.FromJsonOverwrite(jsonStr, m_scrProta);
        
            Player.gameObject.GetComponent<PlayerScript>().enabled = true;
            if (SceneManager.GetActiveScene().name != m_scrProta.actualScene) 
            {
                SceneManager.LoadScene(m_scrProta.actualScene);
                print("HE CARREGAT L'ESCENA");
            }
    }

    public void Respawn() 
    {
        m_scrProta.hp = m_scrProta.max_hp;
        m_scrProta.pintura = m_scrProta.pintura_max;

        modifyHP.Raise();
        modifyPaint.Raise();
        modifyEXP.Raise();

        if (levelManager != null)
        {
            levelManager.SetMaps(m_scrProta);
        }
        Player.GetComponent<PlayerScript>().LoadPosition();
        Player.transform.localScale = new Vector3(1, 1, this.transform.localScale.z);
        Player.GetComponent<PlayerScript>().ChangeState(PlayerScript.Estat.ONAIR);
    }


    public void LoadSceneDead()
    {
        string jsonStr = File.ReadAllText("save.json");
        JsonUtility.FromJsonOverwrite(jsonStr, m_scrProta);
        Player.gameObject.GetComponent<PlayerScript>().enabled = true;
        if (SceneManager.GetActiveScene().name != m_scrProta.actualScene)
        {
            SceneManager.LoadScene(m_scrProta.actualScene);
        }
        m_scrProta.hp = m_scrProta.max_hp;
        m_scrProta.pintura = m_scrProta.pintura_max;
        modifyHP.Raise();
        modifyPaint.Raise();
        modifyEXP.Raise();

        levelManager.SetMaps(m_scrProta);

        Player.transform.position = m_scrProta.position;
        Player.transform.localScale = new Vector3(1, 1, this.transform.localScale.z);
        Player.GetComponent<PlayerScript>().ChangeState(PlayerScript.Estat.ONAIR);
        print("acabo de cargar de locos");

    }

    public bool ShowContinueButton()
    {
        string jsonStr = "";
        try
        {
            jsonStr = File.ReadAllText("save.json");
        }
        catch (FileNotFoundException e)
        {
            print("el fitxer no existeix");
        }
        if (jsonStr == "")
        {
            print("el fitxer esta buit");
            return false;
        }
        else
        {
            print("el fitxer NO esta buit");
            return true;
        }
    }

    public void NewGame()
    {
        m_scrProta.hp = m_scrProta.max_hp;
        m_scrProta.pintura = m_scrProta.pintura_max;
        m_scrProta.weaponColor = WeaponColor.None;
        m_scrProta.spd = m_scrProta.spdOriginal;
        m_scrProta.atk = m_scrProta.atkOriginal;
        m_scrProta.lvl = 1;
        m_scrProta.exp = 1;
        m_scrProta.hasBeenTuto = false;
        m_scrProta.hasBeenNivel1 = false;
        Player.transform.position = m_scrProta.position;
        SceneManager.LoadScene("Tutorial");
    }

    public void ReturnMenu() 
    {
        SceneManager.LoadScene("MainMenu");
    }

}
