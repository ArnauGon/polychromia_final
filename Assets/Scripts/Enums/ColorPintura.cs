using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum ColorPintura
{
    Blanco,
    Negro,
    Rojo,
    Azul,
    Amarillo,
    Morado,
    Naranja,
    Verde
}
