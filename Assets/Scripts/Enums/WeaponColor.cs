using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum WeaponColor 
{ 
    None, 
    Red, 
    Blue, 
    Yellow 
}

