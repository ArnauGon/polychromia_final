using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AutoSaveScript : MonoBehaviour
{
    [SerializeField] GameObject player;
    [SerializeField] ScriptableProta scrProta;
    [SerializeField] GameEvent saveEvent;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "PlayerHurtbox")
        {
            scrProta.position = player.transform.position;
            scrProta.actualScene = SceneManager.GetActiveScene().name;
            saveEvent.Raise();
            this.gameObject.SetActive(false);
        }
    }

}
