using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlipScript : MonoBehaviour
{

    [SerializeField] GameObject enemy;



    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "PlayerHurtbox")
        {
            if (enemy.gameObject.GetComponent<EnemyScript>())
            {
                if (enemy.gameObject.GetComponent<EnemyScript>().stateEnemic == EnemyScript.Estat.PERSEGUIR)
                {
                    enemy.gameObject.GetComponent<EnemyScript>().Girar();
                }
            }
        }
    }

}
