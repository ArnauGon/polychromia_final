using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UISword : MonoBehaviour
{


    [SerializeField] GameObject[] espadas;

    public void ChangeColor(int color, int a, int b, Vector2 vec)
    {
        if (color == 0)
        {
            espadas[0].SetActive(true);
            espadas[1].SetActive(false);
            espadas[2].SetActive(false);
            espadas[3].SetActive(false);
        }

        if (color == 1)
        {
            espadas[0].SetActive(false);
            espadas[1].SetActive(true);
            espadas[2].SetActive(false);
            espadas[3].SetActive(false);
        }

        if (color == 2)
        {
            espadas[0].SetActive(false);
            espadas[1].SetActive(false);
            espadas[2].SetActive(true);
            espadas[3].SetActive(false);            
        }

        if (color == 3)
        {
            espadas[0].SetActive(false);
            espadas[1].SetActive(false);
            espadas[2].SetActive(false);
            espadas[3].SetActive(true);            
        }
    }


}
