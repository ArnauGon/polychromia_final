using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBallScript : MonoBehaviour
{

    [SerializeField] int dmg;


    private void Start()
    {
        this.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;
        this.GetComponent<Rigidbody2D>().gravityScale = 0;
    }

    public void Activate() 
    {
        this.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, -15);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.gameObject.tag == "EnemyHurtbox")
        {
            if (collision.GetComponent<EnemyScript>())
            {
                collision.GetComponent<EnemyScript>().RecieveDmg(dmg, 0, 0, this.gameObject);
                Destroy(this.gameObject);
            }
        }
        if (collision.gameObject.tag == "Land")
        {
            Destroy(this.gameObject);
        }

    }

}
