using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LastRoomScript : MonoBehaviour
{

    [SerializeField] GameObject[] luces;
    [SerializeField] AudioClip musicaBoss;
    [SerializeField] GameObject cameramusic;


    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "PlayerHurtbox")
        {
            cameramusic.GetComponent<AudioSource>().Stop();
            foreach (GameObject lucesfinal in luces)
            {
                lucesfinal.SetActive(true);
            }
        }
        if (collision.gameObject.tag == "EnemyHurtbox")
        {
            this.gameObject.SetActive(false);
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "PlayerHurtbox")
        {
            cameramusic.GetComponent<AudioSource>().PlayOneShot(musicaBoss, 0.5f);
            this.gameObject.SetActive(false);
        }
    }
}
