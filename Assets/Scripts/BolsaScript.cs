using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class BolsaScript : MonoBehaviour
{

    [SerializeField]
    UnityEngine.Color colorpelota;
    public UnityEngine.Color Colorpelota { get => colorpelota; set => colorpelota = value; }

    public GameObject manchaPintura;

    public ScriptablePinturas scriptablePintura;

    public void Start()
    {
        this.gameObject.GetComponent<SpriteRenderer>().color = Colorpelota;
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Land") {
            Destroy(this.gameObject);
            SpawnEffectsFloor(collision);
        }

        if (collision.gameObject.tag == "EnemyHurtbox")
        {
            EnemyCollision(collision);
        }
    }

    private void EnemyCollision(Collision2D collision) 
    {
        if (collision.gameObject.GetComponent<EnemyScript>()) {
            if (this.scriptablePintura.colorsPinturas == ColorPintura.Rojo)
            {
                collision.gameObject.GetComponent<EnemyScript>().RedBody(this.scriptablePintura.color);
            }

            if (this.scriptablePintura.colorsPinturas == ColorPintura.Azul)
            {
                collision.gameObject.GetComponent<EnemyScript>().BlueBody(this.scriptablePintura.color);
            }

            if (this.scriptablePintura.colorsPinturas == ColorPintura.Amarillo)
            {
                collision.gameObject.GetComponent<EnemyScript>().YellowBody(this.scriptablePintura.color);
            }

            Destroy(this.gameObject);
        }
    }

    private void SpawnEffectsFloor(Collision2D collision)
    {
        GameObject effect = Instantiate(scriptablePintura.effect);
        effect.transform.position = this.transform.position;
        //Transforma l'angle en Y segons la normal de la colisi�
        effect.transform.eulerAngles = new Vector3(0, 0, Vector2.SignedAngle(Vector2.up, collision.GetContact(0).normal));

        effect.gameObject.GetComponentInChildren<ManchaScript>().collision = collision;

        Destroy(effect, scriptablePintura.duration);
    }

}



