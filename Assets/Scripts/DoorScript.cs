using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorScript : MonoBehaviour
{
    [SerializeField] int KeyAmountNeeded;
    [SerializeField] int KeyAmountGranted;

    [SerializeField] int ButtonAmountNeeded;
    [SerializeField] int ButtonAmountGranted;

    [SerializeField] bool usesKeys;
    [SerializeField] bool usesButtons;

    [SerializeField] Animator anim;

    GameObject[] KeysInserted;

    private void Start()
    {
        GameObject[] KeysInserted = new GameObject[KeyAmountNeeded];
    }
    private void OpenDoorKey(Collider2D collision)
    {
        if (KeyAmountGranted >= KeyAmountNeeded)
        {
            anim.Play("OpenDoor");
        }
    }

    public void OpenDoorTarget()
    {
        anim.Play("OpenDoor");
    }

    public void TryOpenDoorButtons()
    {
        if (usesButtons)
        {
            ButtonAmountGranted++;
            if (ButtonAmountGranted >= ButtonAmountNeeded)
            {
                anim.Play("OpenDoor");
            }
        }
    }

    public void LeaveButton()
    {
        ButtonAmountGranted--;
        if (ButtonAmountGranted < 0)
        {
            ButtonAmountGranted = 0;
        }

        if (ButtonAmountGranted < ButtonAmountNeeded)
        {
            anim.Play("CloseDoor");
        }
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Key")
        {
            if (usesKeys) {

                print("LA COLISIO DE KEY ES : " + collision.gameObject.name);
                KeyAmountGranted++;
                collision.gameObject.GetComponent<SpriteRenderer>().sprite = null;
                OpenDoorKey(collision);

            }
        }
    }



}
