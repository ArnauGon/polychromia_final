using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlueFloor : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "PurpleMancha" || collision.gameObject.tag == "GreenMancha")
        {
            Destroy(this.gameObject);
        }
    }
}
