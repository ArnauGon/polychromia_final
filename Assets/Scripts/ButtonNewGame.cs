using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonNewGame : MonoBehaviour
{
    [SerializeField] SaveManager save;
    [SerializeField] GameObject ContinueButton;


    private void Start()
    {
        ContinueButton.SetActive(save.ShowContinueButton()); 
    }

    public void Continue()
    {
        save.LoadGame();
    }

    public void NewGame()
    {
        save.NewGame();
    }

    public void Settings()
    {
        SceneManager.LoadScene("Settings");
    }

    public void Exit()
    {
        Application.Quit();
    }
}
