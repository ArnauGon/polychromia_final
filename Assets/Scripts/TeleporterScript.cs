using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleporterScript : MonoBehaviour
{

    [SerializeField] Transform tpPlace;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "PlayerHurtbox") 
        {
            collision.gameObject.transform.position = tpPlace.position;
        }
    }

}
