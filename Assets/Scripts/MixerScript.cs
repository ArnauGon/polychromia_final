using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MixerScript : MonoBehaviour
{

    [SerializeField] int nPintura;
    [SerializeField] ScriptableProta scrProta;


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "PlayerHurtbox")
        {
            if (nPintura == 1)
            {
                scrProta.hasPurple = true;
                Destroy(this.gameObject);
            }
            if (nPintura == 2)
            {
                scrProta.hasOrange = true;
                Destroy(this.gameObject);
            }
            if (nPintura == 3)
            {
                scrProta.hasGreen = true;
                Destroy(this.gameObject);
            }
        }
    }


}
