using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneChanger : MonoBehaviour
{
    [SerializeField] ScriptableProta scrProta;
    [SerializeField] string newScene;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "PlayerHurtbox") 
        {
            SceneManager.LoadScene(newScene);
        }
    }
}
