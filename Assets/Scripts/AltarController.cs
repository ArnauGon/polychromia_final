using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AltarController : MonoBehaviour
{

    [SerializeField] Sprite completedSprite;
    [SerializeField] FinalDoorController finalDoor;

    public void HasBeenCompleted() 
    {
        this.gameObject.GetComponent<SpriteRenderer>().sprite = completedSprite;
        finalDoor.TryOpen();
    }

}
