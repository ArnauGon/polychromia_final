using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetMap : MonoBehaviour
{
    [SerializeField]
    bool Activate;

    [SerializeField]
    GameObject map;

    [SerializeField]
    int nMap;

    [SerializeField] ScriptableProta scrProta;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "PlayerHurtbox")
        {
            if (Activate)
            {
                map.SetActive(true);
                SaveMap(true);
            }

            if (!Activate)
            {
                map.SetActive(false);
                SaveMap(false);
            }
        }
    }

    public void SaveMap(bool set)
    {
        scrProta.maps[nMap] = set;
    }

}
