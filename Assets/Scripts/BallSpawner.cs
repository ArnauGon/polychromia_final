using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallSpawner : MonoBehaviour
{

    [SerializeField] GameObject spawnPoint;
    public Pool pool;

    [SerializeField] ParticleSystem gunshot;

    [SerializeField] float spawnForce;

    [SerializeField] float gunRate;

    void Start()
    {
        StartCoroutine(SpawnBola());
    }

    IEnumerator SpawnBola()
    {
      
        GameObject clone = pool.getElement();
        gunshot.transform.position = spawnPoint.transform.position;
        gunshot.Play();
        clone.transform.position = spawnPoint.transform.position;
        clone.GetComponent<Rigidbody2D>().AddForce(spawnPoint.transform.right * spawnForce, ForceMode2D.Impulse);

        if (clone.GetComponent<BolaTrampaScript>())
        {
            clone.GetComponent<BolaTrampaScript>().bolaPool = pool;
        }

        yield return new WaitForSeconds(gunRate);

        StartCoroutine(SpawnBola());

        yield return new WaitForSeconds(0.5f);

        pool.ReturnElement(clone);
    }

}
