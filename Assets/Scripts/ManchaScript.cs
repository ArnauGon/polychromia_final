using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManchaScript : MonoBehaviour
{
    public GameObject m_gameObject;
    [SerializeField] ScriptablePinturas purple;
    [SerializeField] ScriptablePinturas orange;
    [SerializeField] ScriptablePinturas green;
    public ScriptablePinturas pintura;
    public Collision2D collision;
    [SerializeField] ScriptableProta scrProta;
    [SerializeField] Sprite[] listaSprites;
    private void Start()
    {
        this.gameObject.tag = "NewMancha";
        StartCoroutine(CanviTag());
        SetUpSprite();
    }

    IEnumerator CanviTag()
    {
        yield return new WaitForSeconds(1f);
        this.gameObject.tag = "Mancha";
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
            if (collision.gameObject.GetComponent<ManchaScript>()) {
                if (this.pintura.colorsPinturas == ColorPintura.Rojo && collision.gameObject.GetComponent<ManchaScript>().pintura.colorsPinturas == ColorPintura.Azul)
                {
                if (scrProta.hasPurple) 
                {
                    Destroy(m_gameObject);
                    Destroy(collision.gameObject);
                    CreatePaint(purple);
                }
                }

                if (this.pintura.colorsPinturas == ColorPintura.Rojo && collision.gameObject.GetComponent<ManchaScript>().pintura.colorsPinturas == ColorPintura.Amarillo)
                {
                if (scrProta.hasOrange)
                {
                    Destroy(m_gameObject);
                    Destroy(collision.gameObject);
                    CreatePaint(orange);
                }
                }

                if (this.pintura.colorsPinturas == ColorPintura.Azul && collision.gameObject.GetComponent<ManchaScript>().pintura.colorsPinturas == ColorPintura.Amarillo)
                {
                if (scrProta.hasGreen)
                {
                    Destroy(m_gameObject);
                    Destroy(collision.gameObject);
                    CreatePaint(green);
                }
                }
            }

    }

    private void CreatePaint(ScriptablePinturas pintura)
    {
        GameObject effect = Instantiate(pintura.effect);
        effect.transform.position = this.transform.position;
        effect.transform.eulerAngles = new Vector3(0, 0, Vector2.SignedAngle(Vector2.up, collision.GetContact(0).normal));

        Destroy(effect, pintura.duration);
    }

    public void SetUpSprite() 
    {
        int rndSprite = Random.Range(0,5);
        this.GetComponent<SpriteRenderer>().sprite = listaSprites[rndSprite];
        int rndRotation = Random.Range(0, 360);
        this.transform.Rotate(0, 0, rndRotation);
    }
}
