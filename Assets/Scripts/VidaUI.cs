using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class VidaUI : MonoBehaviour
{
    [SerializeField] Image HPBar;
    [SerializeField] ScriptableProta scrProta;

    private void Start()
    {
        HPBar.fillAmount = (float)scrProta.hp / (float)scrProta.max_hp;
    }

    public void ModifyHP()
    {
        HPBar.fillAmount = (float)scrProta.hp/(float)scrProta.max_hp;
    }
}
