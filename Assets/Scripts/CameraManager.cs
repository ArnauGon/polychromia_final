using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour
{

    [SerializeField] private float VelocitatSeguiment = 3f;
    [SerializeField] private Transform target;

    public Texture2D cursorArrow;

    private void Start()
    {
        Cursor.SetCursor(cursorArrow, Vector2.zero, CursorMode.ForceSoftware);
    }

    private void Update()
    {

         Vector3 worldPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
         Vector2 worldPoint2d = new Vector2(worldPoint.x, worldPoint.y);


         Vector3 PosTarget = new Vector3(Mathf.Clamp((target.position.x + worldPoint2d.x) * 0.5f,target.position.x-12,target.position.x+12), Mathf.Clamp((target.position.y + worldPoint2d.y) * 0.5f, target.position.y - 2, target.position.y + 5), -10);
         transform.position = Vector3.Slerp(transform.position, PosTarget, VelocitatSeguiment * Time.deltaTime);

    }
}
