using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreditsController : MonoBehaviour
{
    [SerializeField] GameObject credits;

    public void CreditViewButton()
    {

            credits.SetActive(true);
        
    }

    public void CreditHideButton()
    {

        credits.SetActive(false);

    }
}
