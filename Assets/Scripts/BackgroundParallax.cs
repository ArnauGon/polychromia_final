using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundParallax : MonoBehaviour
{
    [SerializeField]
    private Transform cameraTransform;

    private Vector3 lastPlayerPosition;

    [SerializeField]
    private Vector2 parallaxEffectMultiplier;

    private float textureUnitSizeX;


    private void Start()
    {
        lastPlayerPosition = cameraTransform.position;
        Sprite sprite = GetComponent<SpriteRenderer>().sprite;
        Texture2D texture = sprite.texture;
        textureUnitSizeX = texture.width / sprite.pixelsPerUnit;
    }
    private void LateUpdate()
    {
        Vector3 deltaMovement = cameraTransform.position - lastPlayerPosition;
        
        transform.position += new Vector3(deltaMovement.x * parallaxEffectMultiplier.x , deltaMovement.y * parallaxEffectMultiplier.y, 0);
        lastPlayerPosition = cameraTransform.position;

    }

}
