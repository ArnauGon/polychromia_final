using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class YellowProjectile : MonoBehaviour
{

    Animator animator;
    RigidbodyType2D atkrecived;
    Color originalColor;


    private void Awake()
    {
        animator = this.GetComponent<Animator>();

    }
    void Start()
    {
        originalColor = new Color(this.GetComponent<SpriteRenderer>().color.r, this.GetComponent<SpriteRenderer>().color.g, this.GetComponent<SpriteRenderer>().color.b,100);
    }

    private void OnDestroy()
    {
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag == "EnemyAtk")
        {
            if (collision.gameObject.GetComponent<Rigidbody2D>())
            {
                StartCoroutine(Stun(collision.gameObject));
            }
        }
        if (collision.transform.tag == "EnemyProjectil")
        {
        }
        if (collision.transform.tag == "EnemyHurtbox")
        {
            if (collision.gameObject.GetComponent<EnemyScript>())
            {
                collision.gameObject.GetComponent<EnemyScript>().YellowBody(new Color(219, 211, 67));
            }
        }

        if (collision.transform.tag == "Ball")
        {
            if (collision.gameObject.GetComponent<Rigidbody2D>())
            {
                StartCoroutine(Stun(collision.gameObject));
            }
        }

    }

    IEnumerator Stun(GameObject collisionGm)
    {
        Vector2 originalSpeed = collisionGm.GetComponent<Rigidbody2D>().velocity;

        collisionGm.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;


        collisionGm.GetComponent<SpriteRenderer>().color = new Color(219, 211, 67);

        yield return new WaitForSeconds(1f);

        collisionGm.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;

        collisionGm.GetComponent<SpriteRenderer>().color = new Color(originalColor.r, originalColor.g, originalColor.b);

        collisionGm.GetComponent<Rigidbody2D>().velocity = originalSpeed;
    }
}
