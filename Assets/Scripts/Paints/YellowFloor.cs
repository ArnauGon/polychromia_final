using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class YellowFloor : MonoBehaviour
{

    [SerializeField]
    int spd = 2;

    [SerializeField]
    float elapsedTime;

    Collider2D m_collision;

    private void Update()
    {
        elapsedTime += Time.deltaTime;

        ResetSpeed(m_collision);

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {

        m_collision = collision;

        if (collision.gameObject.tag == "PlayerHurtbox")
        {
            collision.gameObject.GetComponent<PlayerScript>().Slow(spd);
        }

        if (collision.gameObject.tag == "EnemyHurtbox")
        {
            collision.gameObject.GetComponent<EnemyScript>().Slow(spd);
        }

        if (collision.gameObject.tag == "GreenMancha" || collision.gameObject.tag == "OrangeMancha")
        {
            Destroy(this.gameObject);
        }

    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision)
        {
            ResetCollisionSpeed(collision);
        }
    }

    public void ResetCollisionSpeed(Collider2D collision)
    {
        if (collision.gameObject.tag == "PlayerHurtbox")
        {
            if (collision.gameObject.GetComponent<PlayerScript>())
            {
                collision.gameObject.GetComponent<PlayerScript>().SlowReset();
            }
        }

        if (collision.gameObject.tag == "EnemyHurtbox")
        {
            if (collision.gameObject.GetComponent<EnemyScript>())
            {
                collision.gameObject.GetComponent<EnemyScript>().SlowReset();
            }
        }
    }

    public void ResetSpeed(Collider2D collision)
    {
        if (elapsedTime >= 9)
        {
            if (collision)
            {
                ResetCollisionSpeed(collision);
            }
        }

    }



}
