using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PinzellUI : MonoBehaviour
{

    int contadorColors = 0;
    int contadorColorsborrar= 0;

    [SerializeField] ScriptablePinturas[] scrPinturas;
    Color col;
    [SerializeField] GameObject[] UIPinceles;
    [SerializeField] GameObject UIFondoPinceles;

    [SerializeField] Image[] paintbrush;
    [SerializeField] ScriptableCooldown bolsaCooldown;

    public float pow = 2;
    [SerializeField] int contador;

    public void Start()
    {
        col = scrPinturas[contadorColors].color;
        UIFondoPinceles.GetComponent<Image>().color = new Color(col.r, col.g, col.b);
        UIPinceles[contadorColors].SetActive(true);
        contador = 0;
    }


    public void cosas(int color, int b, int c, Vector2 d)
    {
     
        col = scrPinturas[color].color;

        if(color == 0)
        {
            UIPinceles[0].SetActive(true);
            UIPinceles[1].SetActive(false);
            UIPinceles[2].SetActive(false);
            contador = 0;
        }
        if (color == 1)
        {
            UIPinceles[0].SetActive(false);
            UIPinceles[1].SetActive(true);
            UIPinceles[2].SetActive(false);
            contador = 1;
        }
        if (color == 2)
        {
            UIPinceles[0].SetActive(false);
            UIPinceles[1].SetActive(false);
            UIPinceles[2].SetActive(true);
            contador = 2;
        }

        UIFondoPinceles.GetComponent<Image>().color = new Color(col.r, col.g, col.b);  
    }


    //.---------------------DESAPAREIXER BOLSA-------------------------
    public void TirarBolsa()
    {
        StartCoroutine(FadeCooldown(bolsaCooldown, paintbrush[contador]));
    }

    private IEnumerator FadeCooldown(ScriptableCooldown skill, Image imatge)
    {
        float frequency = 0.1f;
        imatge.color = new Color(imatge.color.r, imatge.color.g, imatge.color.b, 0);
        imatge.fillAmount = 1;

        Debug.Log("color " + imatge.color);
        Debug.Log("imatge " + imatge);

        while (skill.elapsedTime <= skill.cooldown)
        {
            imatge.color = new Color(imatge.color.r, imatge.color.g, imatge.color.b, Mathf.Pow(skill.elapsedTime / skill.cooldown, pow));
            yield return new WaitForSeconds(frequency);
        }

        imatge.color = new Color(imatge.color.r, imatge.color.g, imatge.color.b, 1);
        imatge.fillAmount = 1;
    }


  

}
