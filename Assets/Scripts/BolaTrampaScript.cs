using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BolaTrampaScript : MonoBehaviour
{
    public Pool bolaPool;
    [SerializeField] int dmg;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Land")
        {
            bolaPool.ReturnElement(this.gameObject);
        }
        if (collision.gameObject.tag == "Door")
        {
            bolaPool.ReturnElement(this.gameObject);
        }

        if (collision.gameObject.tag == "PlayerHurtbox")
        {
            if (collision.gameObject.GetComponent<PlayerScript>())
            {
                bolaPool.ReturnElement(this.gameObject);
            }
        }
        if (collision.gameObject.tag == "OrangeSquare")
        {
                bolaPool.ReturnElement(this.gameObject);
        }
    }


}
