using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{

    public static bool GameIsPaused = false;
    [SerializeField] GameObject Player;
    [SerializeField] GameObject PausaMenuUI;
    [SerializeField] GameObject SettingsCanvas;

    [SerializeField] GameObject[] llistActivable;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (GameIsPaused)
            {
                Resume();
            }
            else
            {
                Pausa();
            }
        }
    }


    public void Resume()
    {
        Player.GetComponent<PlayerScript>().enabled = true;
        PausaMenuUI.SetActive(false);
        SettingsCanvas.SetActive(false);
        Time.timeScale = 1f;
        GameIsPaused = false;
        for (int i = 0; i < llistActivable.Length; i++)
        {
            llistActivable[i].SetActive(true);
        }
    }

    public void Pausa()
    {
        Player.GetComponent<PlayerScript>().enabled = false;
        PausaMenuUI.SetActive(true);
        Time.timeScale = 0f;
        GameIsPaused = true;
    }

    public void MainMenu()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene("MainMenu");
    }

    public void Options()
    {
        Player.GetComponent<PlayerScript>().enabled = false;
        PausaMenuUI.SetActive(false);
        SettingsCanvas.SetActive(true);
        Time.timeScale = 0f;
        GameIsPaused = true;
        for (int i = 0; i < llistActivable.Length; i++)
        {
            llistActivable[i].SetActive(false);
        }
    }
}
