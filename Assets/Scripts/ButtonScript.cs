using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonScript : MonoBehaviour
{

    [SerializeField] GameObject target; 
    public void ActivateMechanism()
    {
        switch (target.tag)
        {
            case "Door":
                target.gameObject.GetComponent<DoorScript>().TryOpenDoorButtons();
                break;
        }
    }

    public void DeactivateMechanism()
    {
        switch (target.tag)
        {
            case "Door":
                target.gameObject.GetComponent<DoorScript>().LeaveButton();
                break;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "PlayerHurtbox" || collision.gameObject.tag == "OrangeSquare" || collision.gameObject.tag == "Ball")
        {
            ActivateMechanism();    
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "PlayerHurtbox" || collision.gameObject.tag == "OrangeSquare" || collision.gameObject.tag == "Ball")
        {
            DeactivateMechanism();
        }
    }


}
