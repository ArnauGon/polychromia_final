using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    [SerializeField] GameObject[] maps;


    public void SetMaps(ScriptableProta m_scrProta)
    {
        for (int i = 0; i < maps.Length; i++)
        {
            maps[i].SetActive(m_scrProta.maps[i]);

            print("El mapa : " + maps[i].gameObject.name + " i est� : " + m_scrProta.maps[i]);

        }

    }
}
