using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyScript : MonoBehaviour
{
    [SerializeField] GameObject PlayerBack;
    [SerializeField] CircleCollider2D PickUpCollider;

    bool following;

    private void Start()
    {
        following = false;
        PickUpCollider.enabled = true;
    }
    private void Update()
    {
        if (following)
        {
            FollowPlayer();
        }
    }

    private void FollowPlayer()
    {
        this.transform.position = Vector2.Lerp(this.transform.position,new Vector2(PlayerBack.transform.position.x, PlayerBack.transform.position.y + 1),2f * Time.deltaTime);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "PlayerHurtbox")
        {
            if (collision.gameObject.GetComponent<PlayerScript>())
            {
                following = true;
                PickUpCollider.radius = 7;
            }
        }
    }

}
