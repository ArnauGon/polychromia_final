using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireRainScipt : MonoBehaviour
{
    [SerializeField] GameObject Fire1;
    [SerializeField] GameObject Fire2;
    [SerializeField] GameObject Fire3;
    [SerializeField] GameObject Fire4;
    [SerializeField] GameObject Fire5;
    void Start()
    {
        Fire1.SetActive(false);
        Fire2.SetActive(false);
        Fire3.SetActive(false);
        Fire4.SetActive(false);
        Fire5.SetActive(false);

        StartCoroutine(SpawnFire());
    }

    IEnumerator SpawnFire() 
    {
        Fire3.SetActive(true);
        yield return new WaitForSeconds(0.3f);
        Fire2.SetActive(true);
        Fire4.SetActive(true);
        yield return new WaitForSeconds(0.3f);
        Fire1.SetActive(true);
        Fire5.SetActive(true);
        yield return new WaitForSeconds(0.2f);
        Fire1.GetComponent<FireBallScript>().Activate();
        Fire2.GetComponent<FireBallScript>().Activate();
        Fire3.GetComponent<FireBallScript>().Activate();
        Fire4.GetComponent<FireBallScript>().Activate();
        Fire5.GetComponent<FireBallScript>().Activate();
    }

}
