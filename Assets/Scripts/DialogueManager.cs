using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DialogueManager : MonoBehaviour
{

    bool canActivateDialog;
    private bool didDialogueStart;
    private int lineIndex = 0;

    [SerializeField]GameObject exlamationMark;
    [SerializeField] GameEvent ActivateDesactivatePlayer;
    [SerializeField] GameObject dialoguePanel;
    [SerializeField] GameObject dialogueCharacter;
    [SerializeField] TMP_Text textinGame;
    [SerializeField] string[] dialogue;


    private void Update()
    {
        if(canActivateDialog && Input.GetKeyDown(KeyCode.E))
        {
            if (!didDialogueStart)
            {
                StartDialogue();
            }
            else
            {
                NextLine();
            }
        }
    }

    private void StartDialogue()
    {
        didDialogueStart = true;
        exlamationMark.SetActive(false);
        dialoguePanel.SetActive(true);
        dialogueCharacter.SetActive(true);
       
        ActivateDesactivatePlayer.Raise();
        textinGame.text = dialogue[lineIndex];
    }

    private void NextLine()
    {
        if(lineIndex+2 <= dialogue.Length)
        {
            lineIndex++;
            textinGame.text = dialogue[lineIndex];
        }
        else
        {
            didDialogueStart = false;
            exlamationMark.SetActive(true);
            dialoguePanel.SetActive(false);
            dialogueCharacter.SetActive(false);
            ActivateDesactivatePlayer.Raise();
            lineIndex = 0;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "PlayerHurtbox")
        {
            canActivateDialog = true;
            exlamationMark.SetActive(true);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "PlayerHurtbox")
        {
            canActivateDialog = false;
            exlamationMark.SetActive(false);
        }
        if (didDialogueStart)
        {
            didDialogueStart = false;
            exlamationMark.SetActive(true);
            dialoguePanel.SetActive(false);
            dialogueCharacter.SetActive(false);
            ActivateDesactivatePlayer.Raise();
            lineIndex = 0;
        }
    }
}
