using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PinturaUI : MonoBehaviour
{

    [SerializeField] Image PaintImage;
    [SerializeField] ScriptableProta scrProta;

    public void Start()
    {
        this.PaintImage.fillAmount = (float)scrProta.pintura / (float)scrProta.pintura_max;
    }

    public void ModifyPaint()
    {
        this.PaintImage.fillAmount = (float)scrProta.pintura / (float)scrProta.pintura_max;
    }

}
