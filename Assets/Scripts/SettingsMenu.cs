using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class SettingsMenu : MonoBehaviour
{

    public AudioMixer audioMixer;

    public TMP_Dropdown resolutionDropdown;

    Resolution[] resolutions;

    private float defaultVolume = 1;

    public Slider volumeSlider;

    public Toggle fullscreenToggle;

    public TMP_Dropdown graphicsDropdown;

    int currentResolutionIndex = 0;

    [SerializeField] PauseMenu pauseMenu;
    void Start()
    {

        QualitySettings.SetQualityLevel(5);

        graphicsDropdown.value = 5;

        resolutions = Screen.resolutions;

        resolutionDropdown.ClearOptions();

        List<string> options = new List<string>();

       

        for(int i = 0; i < resolutions.Length; i++)
        {
            string option = resolutions[i].width + " X " + resolutions[i].height;
            options.Add(option);

            if (resolutions[i].width == Screen.currentResolution.width && resolutions[i].height == Screen.currentResolution.height)
            {
                currentResolutionIndex = i;
            }

        }

        resolutionDropdown.AddOptions(options);
        resolutionDropdown.value = currentResolutionIndex;
        resolutionDropdown.RefreshShownValue();      
    }

    public void SetVolume(float volume)
    {

        audioMixer.SetFloat("volume", volume);

    }

    public void SetQuality (int qualityIndex)
    {
        QualitySettings.SetQualityLevel(qualityIndex);
    }

    public void SetFullScreen(bool isFullscreen)
    {
        Screen.fullScreen = isFullscreen;
    }

    public void SetResolution(int resolutionIndex)
    {
        Resolution resolution = resolutions[resolutionIndex];
        Screen.SetResolution(resolution.width, resolution.height, Screen.fullScreen);
    }

    public void backButton()
    {
        if (SceneManager.GetActiveScene().name == "Settings")
        {
            SceneManager.LoadScene("MainMenu");
        }
        else
        {
            this.gameObject.SetActive(false);
            pauseMenu.Pausa();
        }
    }

    public void defaultButton()
    {

        audioMixer.SetFloat("volume", defaultVolume);

        volumeSlider.value = defaultVolume;


        fullscreenToggle.isOn = true;


        Screen.fullScreen = true;


        QualitySettings.SetQualityLevel(5);

        graphicsDropdown.value = 5;


        Screen.SetResolution(1920, 1080, Screen.fullScreen);

        resolutionDropdown.value = currentResolutionIndex;

    }

}
