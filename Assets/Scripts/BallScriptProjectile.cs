using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallScriptProjectile : MonoBehaviour
{
    [SerializeField] ScriptableProta scrProta;
    Rigidbody2D rb;
    Animator animator;

    private void Awake()
    {
        animator = this.GetComponent<Animator>();
        rb = this.GetComponent<Rigidbody2D>();
    }

    public void explosion()
    {
        this.animator.Play("FireBallExplosion");
        this.rb.velocity = new Vector2(0, 0);
        Destroy(this.gameObject, 0.3f);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "EnemyHurtbox")
        {
            if (collision.GetComponent<EnemyScript>())
            {
                explosion();
                collision.GetComponent<EnemyScript>().RecieveDmg(4, 5, 5,  this.gameObject);
            }
        }
        if (collision.gameObject.tag == "Land")
        {
            explosion();
        }

    }
}
