using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class ExpUI : MonoBehaviour
{
    [SerializeField] Image exp_image;
    [SerializeField] float pow = 2;
    [SerializeField] TMP_Text lvl_text;
    [SerializeField] ScriptableProta scrProta;


    public void Start()
    {
        this.exp_image.fillAmount = (float)scrProta.exp / 100;
        lvl_text.text = "LVL:" + scrProta.lvl;
    }

    public void RaiseEXP()
    {
        this.exp_image.fillAmount = (float)scrProta.exp / 100;
        lvl_text.text = "LVL:" + scrProta.lvl;
    }

}
