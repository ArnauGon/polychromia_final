using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "Prota", menuName = "ScriptableObjects/ScriptableProta", order = 1)]
public class ScriptableProta : ScriptableObject
{

    public int max_hp;
    public int hp;
    public int atk;
    public int atkOriginal;
    public float spdOriginal;
    public float spd;
    public int pintura_max;
    public int pintura;
    public int exp;
    public int lvl;
    public WeaponColor weaponColor;
    public Vector2 position;
    public bool hasBeenTuto;
    public bool hasBeenNivel1;
    public string actualScene;
    public bool hasPurple;
    public bool hasOrange;
    public bool hasGreen;
    public bool[] maps; //maps

}
