using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Pinturas", menuName = "ScriptableObjects/ScriptablePinturas", order = 1)]
public class ScriptablePinturas : ScriptableObject
{
    public ColorPintura colorsPinturas;
    public Color color;

    public GameObject effect;
    public float duration;
}
