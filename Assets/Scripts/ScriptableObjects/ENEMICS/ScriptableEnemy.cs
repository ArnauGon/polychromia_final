using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Enemy", menuName = "ScriptableObjects/EnemyScriptable", order = 1)]
public class ScriptableEnemy : ScriptableObject
{
    public int hp;
    public int dmg;
    public ColorPintura color;
}
