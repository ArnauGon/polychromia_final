using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObjects/Cooldown")]
public class ScriptableCooldown : ScriptableObject
{
    public float cooldown;
    public float elapsedTime;
}