using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "Fuentes", menuName = "ScriptableObjects/ScriptableFuentes", order = 1)]
public class ScriptableFuentes : ScriptableObject
{

    public TipusFuente tipusdeFont;
    public int quantitat_restant;
}

