using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.SceneManagement;

public class DeathCameraUI : MonoBehaviour
{

    [SerializeField] GameObject deathCam;
    [SerializeField] GameObject Player;
    [SerializeField] ScriptableProta m_scrProta;
    [SerializeField] GameEvent modifyHP;
    [SerializeField] GameEvent modifyPaint;
    [SerializeField] GameEvent modifyEXP;
    [SerializeField] GameEvent respawn;

    public void ActivateDeathCam()
    {
        deathCam.gameObject.SetActive(true);
        deathCam.GetComponent<Animator>().Play("OnDead");
    }

    public void Respawn()
    {
        deathCam.SetActive(false);
        respawn.Raise();
    }

    public void returnMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }



}
