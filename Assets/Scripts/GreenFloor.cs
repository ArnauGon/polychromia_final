using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GreenFloor : MonoBehaviour
{
    [SerializeField] ScriptablePinturas scrVerde;

    bool playerSmall;
    bool enemySmall;

    private void Start()
    {
        playerSmall = false;
        enemySmall = false;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "PlayerHurtbox")
        {
            if (collision.gameObject.GetComponent<PlayerScript>())
            {
                if (!playerSmall)
                {
                    playerSmall = true;
                    collision.gameObject.GetComponent<PlayerScript>().GreenFloor(scrVerde.duration);
                }
            }
        }

        if (collision.gameObject.tag == "EnemyHurtbox")
        {
            if (!enemySmall)
            {
                enemySmall = true;
                collision.gameObject.GetComponent<EnemyScript>().GreenFloor(scrVerde.duration);
            }
        }

    }


}
