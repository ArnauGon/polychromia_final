using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RedFloorScript : MonoBehaviour
{

    private void Start()
    {
        StartCoroutine(ActiveFire());
    }

    IEnumerator ActiveFire() {
        this.GetComponent<BoxCollider2D>().enabled = true;
        yield return new WaitForSeconds(0.3f);
        this.GetComponent<BoxCollider2D>().enabled = false;
        yield return new WaitForSeconds(1f);
        StartCoroutine(ActiveFire());
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "PlayerHurtbox") {
            collision.gameObject.GetComponent<PlayerScript>().RecieveDmg(5, this.gameObject);
        }

        if (collision.gameObject.tag == "EnemyHurtbox")
        {
            collision.gameObject.GetComponent<EnemyScript>().RecieveDmg(5, 4, 4, this.gameObject);
        }

    }

}
