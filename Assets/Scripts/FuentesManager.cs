using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FuentesManager : MonoBehaviour
{

    [SerializeField] ScriptableFuentes scrFuente;

    [SerializeField] GameEvent eventRecuperarPintura;
    [SerializeField] GameEvent eventRecuperarVida;

    int quantitat;
    TipusFuente tipusfont;

    [SerializeField] ScriptableProta scrProta;

    private void Awake()
    {
        quantitat = scrFuente.quantitat_restant;
        tipusfont = scrFuente.tipusdeFont;
    }


    public void comportamentFont()
    {
 
            if(tipusfont == TipusFuente.Vida) 
            {
                scrProta.hp = scrProta.max_hp;
                eventRecuperarVida.Raise();
            }
            else if (tipusfont == TipusFuente.Pintura)
            {
                scrProta.pintura = scrProta.pintura_max;
                eventRecuperarPintura.Raise();
            }
        }

}
