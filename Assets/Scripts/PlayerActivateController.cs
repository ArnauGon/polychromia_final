using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerActivateController : MonoBehaviour
{
    [SerializeField] PlayerScript scriptPrincipal;
    
    void Awake()
    {
        scriptPrincipal = this.GetComponent<PlayerScript>();
    }

    public void ActivateDesactivateScriptPlayer()
    {

        if (scriptPrincipal.isActiveAndEnabled)
        {
            scriptPrincipal.enabled = false;
        }
        else
        {
            scriptPrincipal.enabled = true;
        }

    }

}
