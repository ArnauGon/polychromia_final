using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrystalScript : MonoBehaviour
{

    [SerializeField] AltarController altar;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "PlayerHurtbox") 
        {
            altar.HasBeenCompleted();
            this.gameObject.SetActive(false);
        }
    }

}
