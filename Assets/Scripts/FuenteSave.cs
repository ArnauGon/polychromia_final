using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FuenteSave : MonoBehaviour
{

    [SerializeField] GameEvent save;


    public void FuenteSaveGame() 
    {
        save.Raise();
    }

}
