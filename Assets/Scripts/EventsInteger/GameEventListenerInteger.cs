using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public class GameEventListenerInteger : MonoBehaviour
{
    [Tooltip("Event to register with.")]
    public GameEventInteger Event;


    [Tooltip("Response to invoke when Event is raised.")]
    public UnityEvent<int, int, int, Vector2> Response;

    private void OnEnable()
    {
        Event.RegisterListener(this);
    }

    private void OnDisable()
    {
        Event.UnregisterListener(this);
    }

    public void OnEventRaised(int value, int value2, int value3, Vector2 value4)
    {
        Response.Invoke(value, value2, value3, value4);
    }
}
