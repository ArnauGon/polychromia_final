using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObjects/Events/GameEventInteger")]
public class GameEventInteger : ScriptableObject
{
    private readonly List<GameEventListenerInteger> eventListeners =
        new List<GameEventListenerInteger>();

    public void Raise(int value, int value2, int value3, Vector2 value4)
    {
        for (int i = eventListeners.Count - 1; i >= 0; i--)
            eventListeners[i].OnEventRaised(value, value2, value3, value4);
    }

    public void RegisterListener(GameEventListenerInteger listener)
    {
        if (!eventListeners.Contains(listener))
            eventListeners.Add(listener);
    }

    public void UnregisterListener(GameEventListenerInteger listener)
    {
        if (eventListeners.Contains(listener))
            eventListeners.Remove(listener);
    }
}

