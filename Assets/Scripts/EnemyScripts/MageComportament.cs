using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MageComportament : EnemyScript
{

    Coroutine attack = null;

    [SerializeField]
    ScriptableEnemy enemyScript;

    [SerializeField]
    private GameObject dropBlockAtt;

    [SerializeField]
    private GameObject iceAtt;

    private int randomAtaque;
    [SerializeField] AudioClip mageatk;
    [SerializeField] AudioClip mageatk2;
    [SerializeField] AudioSource audio_source;


    private void Awake()
    {
        audio_source.GetComponent<AudioSource>();
        sprite = gameObject.GetComponent<SpriteRenderer>();
        this.hpMax = enemyScript.hp;
        this.hp = enemyScript.hp;
        this.dmg = enemyScript.dmg;
        this.originalSpeed = this.speed;
        target = GameObject.FindGameObjectWithTag("PlayerHurtbox").GetComponent<Transform>();
        rb = this.GetComponent<Rigidbody2D>();
        randomEXP = Random.Range(3, 8);
        randomHP = Random.Range(2, 8);
        randomPintura = Random.Range(2, 8);
    }

    private void Start()
    {

    }

    //////////////////////////////////////////////////////////   So   //////////////////////////////////////////////////////////

    void atac1vfx()
    {
        audio_source.PlayOneShot(mageatk);
    }
    void atac2vfx()
    {
        audio_source.PlayOneShot(mageatk2);
    }

    //////////////////////////////////////////////////////////   Estats   //////////////////////////////////////////////////////////

    public override void Patrullar()
    {
        attack = null;
        int direction = 1;
        if (transform.right.x < 0)
            direction = -1;
        rb.velocity = new Vector2(direction * speed, rb.velocity.y);
        animator.Play("MoveMage");
        CheckMuro();

    }

    public override void Perseguir()
    {
        attack = null;
        animator.Play("MoveMage");

        if (Vector2.Distance(transform.position, target.position) > 2)
        {
            transform.position = Vector2.MoveTowards(transform.position, target.position, speed * Time.deltaTime);
        }

        if (this.transform.position.x > player.transform.position.x)
        {
            transform.rotation = Quaternion.Euler(0, 180, 0);
            dretaMov = false;
        }
        else if (this.transform.position.x < player.transform.position.x)
        {
            transform.rotation = Quaternion.Euler(0, 0, 0);
            dretaMov = true;
        }

    }
    public override void agressiu()
    {
        if (attack == null)
        {
            randomAttack();
        }
        if (this.transform.position.x > player.transform.position.x)
        {
            transform.rotation = Quaternion.Euler(0, 180, 0);
            dretaMov = false;
        }
        else if (this.transform.position.x < player.transform.position.x)
        {
            transform.rotation = Quaternion.Euler(0, 0, 0);
            dretaMov = true;
        }
    }

    //////////////////////////////////////////////////////////   Atacs   //////////////////////////////////////////////////////////

    IEnumerator dropBlockAttack()
    {
        animator.Play("AttackMage1");

        Instantiate(dropBlockAtt, new Vector2(player.transform.position.x, player.transform.position.y + 8), Quaternion.identity);

        yield return new WaitForSeconds(1.15f);
        animator.Play("IdleMage");

        StartCoroutine(cdAttack());

        hasAttacked = false;
    }

    IEnumerator iceAttack()
    {
        Vector2 positionPlayer = (Vector2)target.transform.position + (Vector2.up /2);
        Vector2 direction = (Vector2)transform.position - positionPlayer;
        float directionX = 1;
        Vector2 initialPosition = transform.position;
        if (direction.x < 0)
        {
            directionX = -1;
        }

        direction.x -= directionX * 2;
        initialPosition.x -= directionX * 2;

        animator.Play("AttackMage2");

        GameObject projectil = Instantiate(iceAtt, initialPosition, Quaternion.Euler(0,0, 180+Mathf.Rad2Deg*Mathf.Atan(direction.y/direction.x)));

        projectil.gameObject.GetComponent<Rigidbody2D>().velocity = projectil.transform.right * directionX  * 10;
        yield return new WaitForSeconds(1.3f);
        animator.Play("IdleMage");

        StartCoroutine(cdAttack());

        hasAttacked = false;

    }



    IEnumerator cdAttack()
    {
        if (stateSaved == Estat.AGRESSIU)
        {
            yield return new WaitForSeconds(1.3f);

            attack = null;
            stateEnemic = stateSaved;
            hasAttacked = false;
        }

        if (stateSaved == Estat.PERSEGUIR)
        {
            yield return new WaitForSeconds(0.3f);

            stateEnemic = stateSaved;
            hasAttacked = false;
        }

    }

    void randomAttack()
    {
        randomAtaque = Random.Range(1, 3);

        switch (randomAtaque)
        {
            case 1:

                attack = StartCoroutine(dropBlockAttack());
                break;

            case 2:

                attack = StartCoroutine(iceAttack());
                break;
        }

    }

    //////////////////////////////////////////////////////////   Altres   //////////////////////////////////////////////////////////

    public override void Girar()
    {
        if (dretaMov == true)
        {
            transform.rotation = Quaternion.Euler(0, 0, 0);
        }
        if (dretaMov == false)
        {
            transform.rotation = Quaternion.Euler(0, 180, 0);
        }
    }

    public override void ResetRogue()
    {

    }
}
