using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public abstract class EnemyScript : MonoBehaviour
{


    #region Variables

    //////////////////////////////////////////////////////////   Variables   //////////////////////////////////////////////////////////


    [SerializeField] GameObject particulasMuerteEne;

    [SerializeField] protected GameObject player;

    protected Transform target;

    public GameObject floatingDamage;

    [SerializeField] private ScriptableProta scrProta;

    [SerializeField] protected bool dretaMov = true;

    protected Vector3 moverse;

    protected float distanciaRaycast = 1f;
    [SerializeField] protected LayerMask layer;

    protected SpriteRenderer sprite;
    protected float originalSpeed;
    protected int hpMax;
    protected int hp;
    protected int dmg;
    protected int xForce = 5;
    [SerializeField] protected Rigidbody2D rb;
    [SerializeField] protected float speed;

    [SerializeField] GameEventInteger eventMostrarDMG;
    [SerializeField] protected GameObject exp_object;
    [SerializeField] protected GameObject hp_object;
    [SerializeField] protected GameObject pintura_object;

    [SerializeField] GameEventInteger MostrarPopUp;

    [SerializeField] Color colorOriginal;
    [SerializeField] GameObject[] hitboxes;
    [SerializeField] ParticleSystem BlueBurstParticle;
    [SerializeField] GameObject RedRain;

    [SerializeField] protected Animator animator;
    protected int randomEXP;
    protected int randomHP;
    protected int randomPintura;

    private bool isStunned;



    [SerializeField]
    protected bool hasAttacked = false;


    [SerializeField] DoorScript door;

    public enum Estat
    {
        PATRULLA,
        PERSEGUIR,
        AGRESSIU,
        STUN
    }
    
    public Estat stateEnemic = Estat.PATRULLA;

    public Estat stateSaved = Estat.PATRULLA;

    protected int Dmg { get => dmg; set => dmg = value; }

    #endregion

    void Start()
    {
        this.colorOriginal = this.GetComponent<SpriteRenderer>().color;
        this.originalSpeed = speed;
        target = GameObject.FindGameObjectWithTag("PlayerHurtbox").GetComponent<Transform>();
    }

    void Update()
    {
        EstatsEnemics();
    }

    #region Comportaments
    //////////////////////////////////////////////////////////   M�quina D'estats i comportaments  //////////////////////////////////////////////////////////


    void EstatsEnemics()
    {
        switch (stateEnemic)
        {
            case Estat.PATRULLA:

                Patrullar();

                break;
            case Estat.PERSEGUIR:

                Perseguir();

                break;
            case Estat.AGRESSIU:

                agressiu();

                break;
            case Estat.STUN:
                rb.velocity = new Vector2(0, 0);
                break;
            default:
                break;
        }
    }

    public abstract void Patrullar();
    public abstract void Perseguir();
    public abstract void agressiu();

    public abstract void Girar();

    public abstract void ResetRogue();




    public void ChangeState(Estat nouEstat)
    {
        ExitState(stateEnemic);
        EnterState(nouEstat);
    }

    public void EnterState(Estat nouEstat)
    {
        switch (nouEstat)
        {
            case Estat.PATRULLA:

                break;

            case Estat.PERSEGUIR:

                break;

            case Estat.AGRESSIU:
               
                break;
        }

        stateEnemic = nouEstat;
    }
    public void ExitState(Estat estatActual)
    {

        if(estatActual == Estat.PATRULLA)
        {

        }

         if( estatActual == Estat.PERSEGUIR)
        {

        }

        if(estatActual == Estat.AGRESSIU)
        {

        }

    }


    protected void ChangeIsAttacking()
    {
        hasAttacked = true;
    }


    public void ChangePatrolToFollow()
    {
        if (isStunned == false && stateEnemic == Estat.PATRULLA)
        {
            stateSaved = Estat.PERSEGUIR;
            stateEnemic = Estat.PERSEGUIR;
        }
    }

    public void ChangeFollowToPatrol()
    {
        if (isStunned == false && stateEnemic == Estat.PERSEGUIR)
        {
            StartCoroutine(waitToEnterPatroll());
           
            stateEnemic = stateSaved;
        }
    }

    public void ChangeFollowToAgressive()
    {
        if (isStunned == false && stateEnemic == Estat.PERSEGUIR)
        {
            stateSaved = Estat.AGRESSIU;
            ChangeState(Estat.AGRESSIU);
        }
    }

    public void ChangeAgressiveToFollow()
    {
        if (isStunned == false && stateEnemic == Estat.AGRESSIU && hasAttacked ==  true)
        {
            stateSaved = Estat.PERSEGUIR;
            CheckIfCanChangeState(stateSaved);
 
        }
    }

    public void ChangeAgressiveToPatrol()
    {
        if (isStunned == false && stateEnemic == Estat.AGRESSIU )
        {
            StartCoroutine(waitToExitAgressiveSlime());

            stateEnemic = stateSaved;

        }
    }


    public IEnumerator waitToEnterPatroll()
    {
        if (stateSaved == Estat.PATRULLA)
        {
            yield return new WaitForSeconds(2f);
            stateEnemic = stateSaved;

        }   
    }

    public IEnumerator waitToExitAgressiveSlime()
    {
        if (stateSaved == Estat.PATRULLA)
        {
            yield return new WaitForSeconds(0.5f);
            stateEnemic = stateSaved;

        }
    }


    public void CheckIfCanChangeState(Estat estatSaved)
    {
            if (hasAttacked == false)
            {
                ChangeState(estatSaved);
            }
    }
    protected void CheckMuro()
    {
        if (dretaMov == true)
        {
            rb.velocity = new Vector2(speed, rb.velocity.y);
        }
        else
        {
            rb.velocity = new Vector2(-speed, rb.velocity.y);
        }
        
        RaycastHit2D hit = Physics2D.Raycast(this.transform.position, rb.velocity.normalized, distanciaRaycast, layer);

        if (hit.collider != null)
        {
            if (hit.collider.gameObject.layer == 10)
            {
                dretaMov = !dretaMov;
                Girar();
            }
        }
    }

    IEnumerator Stun()
    {
        isStunned = true;
        Estat estatAnterior = stateSaved;
        ChangeState(Estat.STUN);
        this.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;
        foreach (GameObject hitbox in hitboxes)
        {
            hitbox.SetActive(false);
        }

        yield return new WaitForSeconds(1f);

        isStunned = false;
        this.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
        foreach (GameObject hitbox in hitboxes)
        {
            hitbox.SetActive(true);
        }
        this.GetComponent<SpriteRenderer>().color = new Color(255f, 255f, 255f, 255f);
        ChangeState(estatAnterior);
    }

    #endregion


    #region Pintures
    //////////////////////////////////////////////////////////   Comportaments amb Pintura   //////////////////////////////////////////////////////////

    public void Slow(float n) 
    {
        speed = n;
    }

    public void SlowReset()
    {
        speed = originalSpeed;
    }
    public void RedBody(Color color)
    {
        this.GetComponent<SpriteRenderer>().color = new Color(color.r, color.g, color.b, 255f);
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
        GameObject RedRainClone = Instantiate(RedRain);
        RedRainClone.transform.position = new Vector2(this.transform.position.x,this.transform.position.y + 5);
        Destroy(RedRainClone,2f);
    }

    public void BlueBody(Color color) 
    {
        BlueBurstParticle.transform.position = this.transform.position;
        BlueBurstParticle.Play();
        this.GetComponent<SpriteRenderer>().color = new Color(color.r,color.g,color.b, 255f);
        this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0,10),ForceMode2D.Impulse);
    }

    public void YellowBody(Color color)
    {
        this.GetComponent<SpriteRenderer>().color = new Color(255f, 255f, 0f, 255f);
        StartCoroutine(Stun());
    }

    
    public void GreenFloor(float duration)
    {
        StartCoroutine(GreenFloorTimer(duration));
    }

    IEnumerator GreenFloorTimer(float duration)
    {
        this.gameObject.transform.localScale = new Vector3(this.transform.localScale.x / 2, this.transform.localScale.y / 2, this.transform.localScale.z);

        yield return new WaitForSeconds(duration);
        this.gameObject.transform.localScale = new Vector3(1, 1, this.transform.localScale.z);
    }
    #endregion


    #region Damage i Mort

    //////////////////////////////////////////////////////////   Rebre mal i Mort  //////////////////////////////////////////////////////////

    public void RecieveDmg(int n, int forceX, int forceY, GameObject posDmg) {
        
        hp = hp - n;


        MostrarPopUp.Raise(n, n, n, new Vector2(this.transform.position.x, this.transform.position.y + 1));

        //KnockBack
        int direccio = 1;
        if (transform.position.x < posDmg.transform.position.x)
            direccio = -1;

        rb.AddForce(new Vector2(direccio * forceX, forceY), ForceMode2D.Impulse);
        
        OnDeath();

    }

    void OnDeath()
    {
        if (hp <= 0)
        {
            this.gameObject.layer = 8;
             GameObject partMuerteClone = Instantiate(particulasMuerteEne);
             partMuerteClone.transform.position = this.transform.position;
             partMuerteClone.transform.GetComponent<VisualEffect>().Play();

            this.GetComponent<SpriteRenderer>().color = Color.clear;
            BoxCollider2D collider = this.GetComponent<BoxCollider2D>();
            collider.enabled = false;
            Rigidbody2D rigidEne = this.GetComponent<Rigidbody2D>();
            
            dropOnDead();
            rb.bodyType = RigidbodyType2D.Static;

            if (door != null) 
            {
                door.OpenDoorTarget();
            }

            Destroy(this.gameObject, 0.3f);
              Destroy(partMuerteClone, 3f);
        }
    }
    private void dropOnDead()
    {
        for (int i = 0; i < randomEXP; i++)
        {
            GameObject clone = Instantiate(exp_object, new Vector2(this.transform.position.x + Random.Range(-0.5f, 0.5f), this.transform.position.y), Quaternion.identity);
            clone.GetComponent<Rigidbody2D>().velocity = new Vector2(0, Random.Range(7, 3));
            Vector2 follow = Vector2.MoveTowards(clone.transform.position, player.transform.position, 10 * Time.deltaTime);
            clone.GetComponent<Rigidbody2D>().MovePosition(follow);
        }

        for (int i = 6; i < randomHP; i++)
        {
            GameObject clone = Instantiate(hp_object, new Vector2(this.transform.position.x + Random.Range(-0.5f, 0.5f), this.transform.position.y), Quaternion.identity);
            clone.GetComponent<Rigidbody2D>().velocity = new Vector2(0, Random.Range(7, 3));
            Vector2 follow = Vector2.MoveTowards(clone.transform.position, player.transform.position, 10 * Time.deltaTime);
            clone.GetComponent<Rigidbody2D>().MovePosition(follow);
        }

        for (int i = 6; i < randomPintura; i++)
        {
            GameObject clone = Instantiate(pintura_object, new Vector2(this.transform.position.x + Random.Range(-0.5f, 0.5f), this.transform.position.y), Quaternion.identity);
            clone.GetComponent<Rigidbody2D>().velocity = new Vector2(0, Random.Range(7, 3));
            Vector2 follow = Vector2.MoveTowards(clone.transform.position, player.transform.position, 10 * Time.deltaTime);
            clone.GetComponent<Rigidbody2D>().MovePosition(follow);
        }
    }
    #endregion


    public void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.gameObject.tag == "PlayerHitbox")
        {
            this.RecieveDmg(scrProta.atk, 3, 3, collision.gameObject);
            this.GetComponent<SpriteRenderer>().color = new Color(255f, 255f, 255f, 255f);
        }
        if (collision.gameObject.tag == "WeaponRed")
        {
            this.RecieveDmg(scrProta.atk, 4, 4, collision.gameObject);
            this.GetComponent<SpriteRenderer>().color = new Color(255f, 0f, 0f, 255f);
        }
        if (collision.gameObject.tag == "WeaponBlue")
        {
            this.RecieveDmg(scrProta.atk, 15, 5, collision.gameObject);
            this.GetComponent<SpriteRenderer>().color = new Color(0f, 0f, 255f, 255f);
        }
        if (collision.gameObject.tag == "WeaponYellow")
        {
            this.RecieveDmg(scrProta.atk, 2, 2, collision.gameObject);
            StartCoroutine(Stun());
            this.GetComponent<SpriteRenderer>().color = new Color(255f, 255f, 0f, 255f);
        }
        if (collision.gameObject.tag == "EnemyTurnTrigger")
        {
            CheckMuro();
        }

    }
}
