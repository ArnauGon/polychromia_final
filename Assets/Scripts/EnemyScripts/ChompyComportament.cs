using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChompyComportament : EnemyScript
{

    Coroutine attack = null;

    [SerializeField]
    ScriptableEnemy enemyScript;

    private int randomAtaque;
    [SerializeField] AudioSource audio_source;
    [SerializeField] AudioClip jumpatk;
    private void Awake()
    {
        audio_source.GetComponent<AudioSource>();
        sprite = gameObject.GetComponent<SpriteRenderer>();
        this.hpMax = enemyScript.hp;
        this.hp = enemyScript.hp;
        this.dmg = enemyScript.dmg;
        this.originalSpeed = this.speed;
        target = GameObject.FindGameObjectWithTag("PlayerHurtbox").GetComponent<Transform>();
        rb = this.GetComponent<Rigidbody2D>();
        randomEXP = Random.Range(3, 8);
        randomHP = Random.Range(2, 8);
        randomPintura = Random.Range(2, 8);
    }
    void Start()
    {


    }

    //////////////////////////////////////////////////////////   Estats   //////////////////////////////////////////////////////////

    public override void Patrullar()
    {
        attack = null;
        int direction = 1;
        if (transform.right.x < 0)
            direction = -1;
        rb.velocity = new Vector2(direction * speed, rb.velocity.y);
        animator.Play("MoveSlime");
        CheckMuro();

    }
    public override void Perseguir()
    {
        attack = null;
        animator.Play("MoveSlime");

        if (Vector2.Distance(transform.position, target.position) > 2)
        {
            transform.position = Vector2.MoveTowards(transform.position, target.position, speed * Time.deltaTime);
        }


    }
    public override void agressiu()
    {

        if (attack == null)
        {
            randomAttack();
        }


    }

    //////////////////////////////////////////////////////////   Atacs   //////////////////////////////////////////////////////////

    IEnumerator JumpAttack()
    {
        audio_source.PlayOneShot(jumpatk, 0.20f);

        if (dretaMov == false) {

            animator.Play("AttackSlime");

            this.GetComponent<Rigidbody2D>().velocity = new Vector2(-10, 5);

            yield return new WaitForSeconds(1f);

            animator.Play("IdleSlime");

            StartCoroutine(cdAttack());

            hasAttacked = false;

            stateEnemic = Estat.PATRULLA;
        }
        else
        {

            animator.Play("AttackSlime");

            this.GetComponent<Rigidbody2D>().velocity = new Vector2(10, 5);

            yield return new WaitForSeconds(1f);

            animator.Play("IdleSlime");

            StartCoroutine(cdAttack());

            hasAttacked = false;

            stateEnemic = Estat.PATRULLA;
        }
    }

    void randomAttack()
    {
        randomAtaque = Random.Range(1, 2);

        switch (randomAtaque)
        {
            case 1:

                attack = StartCoroutine(JumpAttack());
                break;

        }
    }


    IEnumerator cdAttack()
    {
        if (stateSaved == Estat.AGRESSIU)
        {
            yield return new WaitForSeconds(1.3f);

            attack = null;
            stateEnemic = stateSaved;
            hasAttacked = false;
        }

        if (stateSaved == Estat.PERSEGUIR)
        {
            yield return new WaitForSeconds(0.3f);

            stateEnemic = stateSaved;
            hasAttacked = false;
        }

    }

    //////////////////////////////////////////////////////////   Altres   //////////////////////////////////////////////////////////

    public override void Girar()
    {
        if (dretaMov == true)
        {
            transform.rotation = Quaternion.Euler(0, 0, 0);
        }
        if (dretaMov == false)
        {
            transform.rotation = Quaternion.Euler(0, 180, 0);
        }
    }

    public override void ResetRogue()
    {
       
    }
}
