using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyAttack : MonoBehaviour
{
    [SerializeField] private GameObject player;

    [SerializeField] private ScriptableEnemy mageScriptable;

    private void Awake()
    {
        Destroy(gameObject, 5f);
    }
    
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.transform.tag == "PlayerHurtbox")
        {
            collision.gameObject.GetComponent<PlayerScript>().RecieveDmg(mageScriptable.dmg, this.gameObject);
            Destroy(this.gameObject, 0.1f);
        }
        else if (collision.collider.gameObject.layer == 10 || collision.transform.tag == "EnemyHurtbox")
        {
            Destroy(this.gameObject, 0.1f);
        }
    }
}
