using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerAlarma : MonoBehaviour
{
    [SerializeField] private EnemyScript enemyScript;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        // Al entrar al estat
        if (collision.gameObject.tag == "PlayerHurtbox" && (enemyScript.stateEnemic == EnemyScript.Estat.PATRULLA || enemyScript.stateEnemic == EnemyScript.Estat.PERSEGUIR))
        {
            enemyScript.stateSaved = EnemyScript.Estat.PERSEGUIR;
            enemyScript.ChangePatrolToFollow();
        }
        else if (collision.gameObject.tag == "PlayerHurtbox" && enemyScript.stateEnemic == EnemyScript.Estat.AGRESSIU)
        {
            enemyScript.stateSaved = EnemyScript.Estat.PERSEGUIR;
            enemyScript.ChangeAgressiveToFollow();
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        // Al sortir del estat
        if (collision.gameObject.tag == "PlayerHurtbox" && enemyScript.stateEnemic == EnemyScript.Estat.PERSEGUIR)
        {
            enemyScript.stateSaved = EnemyScript.Estat.PATRULLA;

            StartCoroutine(enemyScript.waitToEnterPatroll());
        }
    }

}
