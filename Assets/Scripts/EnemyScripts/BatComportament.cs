using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BatComportament : EnemyScript
{

    Coroutine attack = null;

    [SerializeField]
    ScriptableEnemy enemyScript;

    private int randomAtaque;
    [SerializeField] AudioSource audio_source;
    [SerializeField] AudioClip batatk;

    private void Awake()
    {
        audio_source.GetComponent<AudioSource>();
        sprite = gameObject.GetComponent<SpriteRenderer>();
        this.hpMax = enemyScript.hp;
        this.hp = enemyScript.hp;
        this.dmg = enemyScript.dmg;
        this.originalSpeed = this.speed;
        target = GameObject.FindGameObjectWithTag("PlayerHurtbox").GetComponent<Transform>();
        rb = this.GetComponent<Rigidbody2D>();
        randomEXP = Random.Range(3, 8);
        randomHP = Random.Range(2, 8);
        randomPintura = Random.Range(2, 8);
    }

    //////////////////////////////////////////////////////////   Estats   //////////////////////////////////////////////////////////

    public override void Patrullar()
    {
        animator.Play("BatIdle");
        sprite.flipY = true;
        Debug.Log(this.hp);
    }

    public override void Perseguir()
    {
        sprite.flipY = false;
        attack = null;
        animator.Play("BatFly");

        if (Vector2.Distance(transform.position, target.position) > 2)
        {
            transform.position = Vector2.MoveTowards(transform.position, target.position, speed * Time.deltaTime);
        }

        if (this.transform.position.x > player.transform.position.x)
        {
            transform.rotation = Quaternion.Euler(0, 180, 0);
            dretaMov = false;
        }
        else if (this.transform.position.x < player.transform.position.x)
        {
            transform.rotation = Quaternion.Euler(0, 0, 0);
            dretaMov = true;
        }
    }

    public override void agressiu()
    {
        if (attack == null)
        {
            randomAttack();
        }
        if (this.transform.position.x > player.transform.position.x)
        {
            transform.rotation = Quaternion.Euler(0, 180, 0);
            dretaMov = false;
        }
        else if (this.transform.position.x < player.transform.position.x)
        {
            transform.rotation = Quaternion.Euler(0, 0, 0);
            dretaMov = true;
        }
    }

    //////////////////////////////////////////////////////////   Atacs   //////////////////////////////////////////////////////////

    IEnumerator BiteBat()
    {
        animator.Play("BiteBat");

        this.hp += this.dmg / 2;
        yield return new WaitForSeconds(1.2f);
        audio_source.PlayOneShot(batatk, 0.15f);

        if(this.hp > this.hpMax)
        {
            this.hp = this.hpMax;
        }

        animator.Play("BatFly");

        StartCoroutine(cdAttack());

        hasAttacked = false;

    }

    void randomAttack()
    {
        randomAtaque = Random.Range(1, 2);

        switch (randomAtaque)
        {
            case 1:
 
                attack = StartCoroutine(BiteBat());
                break;
        }

    }

    IEnumerator cdAttack()
    {
        if (stateSaved == Estat.AGRESSIU)
        {
            yield return new WaitForSeconds(1.3f);

            attack = null;
            stateEnemic = stateSaved;
            hasAttacked = false;
        }

        if (stateSaved == Estat.PERSEGUIR)
        {
            yield return new WaitForSeconds(0.3f);

            stateEnemic = stateSaved;
            hasAttacked = false;
        }

    }

    //////////////////////////////////////////////////////////   Altres   //////////////////////////////////////////////////////////

    public override void Girar()
    {
        if (dretaMov == true)
        {
            transform.rotation = Quaternion.Euler(0, 0, 0);
        }
        if (dretaMov == false)
        {
            transform.rotation = Quaternion.Euler(0, 180, 0);
        }
    }

    public override void ResetRogue()
    {
    }
}
