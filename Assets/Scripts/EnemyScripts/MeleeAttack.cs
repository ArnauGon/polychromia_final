using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeAttack : MonoBehaviour
{
    

    [SerializeField] private ScriptableEnemy enemyScriptable;



    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag == "PlayerHurtbox")
        {
            collision.gameObject.GetComponent<PlayerScript>().RecieveDmg(this.enemyScriptable.dmg, this.gameObject);
        }
    }
}
