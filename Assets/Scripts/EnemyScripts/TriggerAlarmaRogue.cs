using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerAlarmaRogue : MonoBehaviour
{
    [SerializeField] private EnemyScript enemyScript;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        // Al entrar al estat
        if (collision.gameObject.tag == "PlayerHurtbox" && (enemyScript.stateEnemic == EnemyScript.Estat.PATRULLA || enemyScript.stateEnemic == EnemyScript.Estat.PERSEGUIR))
        {
            enemyScript.stateSaved = EnemyScript.Estat.PERSEGUIR;
            enemyScript.ChangePatrolToFollow();
        }
        else if (collision.gameObject.tag == "PlayerHurtbox" && enemyScript.stateEnemic == EnemyScript.Estat.AGRESSIU)
        {
            enemyScript.stateSaved = EnemyScript.Estat.PERSEGUIR;
            enemyScript.ChangeAgressiveToFollow();
        }
    }

}
