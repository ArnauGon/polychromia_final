using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LancerComportament : EnemyScript
{

    Coroutine attack = null;

    [SerializeField]
    ScriptableEnemy enemyScript;

    private int randomAtaque;
    [SerializeField] AudioSource audio_source;
    [SerializeField] AudioClip lanceratk;
    [SerializeField] AudioClip lanceratk2;

    private void Awake()
    {
        audio_source.GetComponent<AudioSource>();
        sprite = gameObject.GetComponent<SpriteRenderer>();
        this.hpMax = enemyScript.hp;
        this.hp = enemyScript.hp;
        this.dmg = enemyScript.dmg;
        this.originalSpeed = this.speed;
        target = GameObject.FindGameObjectWithTag("PlayerHurtbox").GetComponent<Transform>();
        rb = this.GetComponent<Rigidbody2D>();
        randomEXP = Random.Range(3, 8);
        randomHP = Random.Range(2, 8);
        randomPintura = Random.Range(2, 8);
    }

    private void Start()
    {
     
    }
    //////////////////////////////////////////////////////////   So   //////////////////////////////////////////////////////////
    void soundatk1()
    {
        audio_source.PlayOneShot(lanceratk);
    }
    void soundatk2()
    {
        audio_source.PlayOneShot(lanceratk2);
    }

    //////////////////////////////////////////////////////////   Estats   //////////////////////////////////////////////////////////

    public override void Patrullar()
    {
        attack = null;
        int direction = 1;
        if (transform.right.x < 0)
            direction = -1;
        rb.velocity = new Vector2(direction * speed, rb.velocity.y);
        animator.Play("MovePincelEnemigo");
        CheckMuro();

    }
    public override void Perseguir()
    {
        attack = null;
        animator.Play("MovePincelEnemigo");

        if (Vector2.Distance(transform.position, target.position) > 2)
        {
            transform.position = Vector2.MoveTowards(transform.position, target.position, speed * Time.deltaTime);
        }

        if (this.transform.position.x > player.transform.position.x)
        {
            transform.rotation = Quaternion.Euler(0,180,0);
            dretaMov = false;
        }
        else if (this.transform.position.x < player.transform.position.x)
        {
            transform.rotation = Quaternion.Euler(0, 0, 0);
            dretaMov = true;
        }

    }

    public override void agressiu()
    {

        if (attack == null)
        {
            randomAttack();
        }
        if (this.transform.position.x > player.transform.position.x)
        {
            transform.rotation = Quaternion.Euler(0, 180, 0);
            dretaMov = false;
        }
        else if (this.transform.position.x < player.transform.position.x)
        {
            transform.rotation = Quaternion.Euler(0, 0, 0);
            dretaMov = true;
        }

    }

    //////////////////////////////////////////////////////////   Atacs   //////////////////////////////////////////////////////////

    void randomAttack()
    {
        randomAtaque = Random.Range(1, 4);

        switch (randomAtaque)
        {
            case 1:
                
                attack = StartCoroutine(Melee1());
                break;

            case 2:
                
                attack = StartCoroutine(Melee2());
                break;

            case 3:
                
                attack = StartCoroutine(SpecialAttack());
                break;
        }
    }


    IEnumerator cdAttack()
    {
        if (stateSaved == Estat.AGRESSIU)
        {
            yield return new WaitForSeconds(1.3f);

            attack = null;
            stateEnemic = stateSaved;
            hasAttacked = false;
        }

        if (stateSaved == Estat.PERSEGUIR)
        {
            yield return new WaitForSeconds(0.3f);

            stateEnemic = stateSaved;
            hasAttacked = false;
        }
     
    }

    IEnumerator Melee1()
    {

        animator.Play("Attack1EP");

        yield return new WaitForSeconds(0.9f);

        animator.Play("IdleLancer");

        StartCoroutine(cdAttack());

        hasAttacked = false;
        
    }

    IEnumerator Melee2()
    {

        animator.Play("Attack2EP");

        yield return new WaitForSeconds(1.05f);

        animator.Play("IdleLancer");

        StartCoroutine(cdAttack());

        hasAttacked = false;
    }

    IEnumerator SpecialAttack()
    {

        animator.Play("SpecialEP");

        yield return new WaitForSeconds(1.3f);

        animator.Play("IdleLancer");

        StartCoroutine(cdAttack());

        hasAttacked = false;
    }

    //////////////////////////////////////////////////////////   Altres   //////////////////////////////////////////////////////////

    public override void Girar()
    {
        if (dretaMov == true)
        {
            transform.rotation = Quaternion.Euler(0, 0, 0);
        }
        if (dretaMov == false)
        {
            transform.rotation = Quaternion.Euler(0, 180, 0);
        }
    }

    public override void ResetRogue()
    {
        
    }
}
