using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerAlarmaBat : MonoBehaviour
{

    [SerializeField] private EnemyScript enemyScript;

    [SerializeField] private Animator animator;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "PlayerHurtbox" && (enemyScript.stateEnemic == EnemyScript.Estat.PATRULLA || enemyScript.stateEnemic == EnemyScript.Estat.PERSEGUIR))
        {
            //  StopCoroutine(enemyScript.waitToEnterPatroll());
            enemyScript.stateSaved = EnemyScript.Estat.PERSEGUIR;
            StartCoroutine(IdleToFollowAnim());
        }
        else if (collision.gameObject.tag == "PlayerHurtbox" && enemyScript.stateEnemic == EnemyScript.Estat.AGRESSIU)
        {
            enemyScript.stateSaved = EnemyScript.Estat.PERSEGUIR;
            enemyScript.ChangeAgressiveToFollow();
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        // le paso la ransform
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        // Salir estado que toque
    }

    IEnumerator IdleToFollowAnim()
    {
        animator.Play("IdleToFlyBat");
        yield return new WaitForSeconds(1f);
        enemyScript.ChangePatrolToFollow();
    }
}
