using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RogueComportament : EnemyScript
{

    Coroutine attack = null;

    [SerializeField]
    ScriptableEnemy enemyScript;

    [SerializeField]
    private GameObject kunai;

    private int randomAtaque;

    [SerializeField] AudioClip rogueatk;
    [SerializeField] AudioClip rogueatk2;
    [SerializeField] AudioClip rogueatk3;
    [SerializeField] AudioSource audio_source;

    private void Awake()
    {
        sprite = gameObject.GetComponent<SpriteRenderer>();
        this.hpMax = enemyScript.hp;
        this.hp = enemyScript.hp;
        this.dmg = enemyScript.dmg;
        this.originalSpeed = this.speed;
        target = GameObject.FindGameObjectWithTag("PlayerHurtbox").GetComponent<Transform>();
        rb = this.GetComponent<Rigidbody2D>();
        randomEXP = Random.Range(3, 8);
        randomHP = Random.Range(2, 8);
        randomPintura = Random.Range(2, 8);
    }


    public override void Patrullar()
    {
        animator.Play("IdleRogue");
        rb.velocity = new Vector2(0, 0);
    }

    //////////////////////////////////////////////////////////   Estats   //////////////////////////////////////////////////////////

    public override void Perseguir()
    {
        attack = null;
        animator.Play("RunRogue");

        if (Vector2.Distance(transform.position, target.position) > 2)
        {
            transform.position = Vector2.MoveTowards(transform.position, target.position, speed * Time.deltaTime);
        }

        if (this.transform.position.x > player.transform.position.x)
        {
            transform.rotation = Quaternion.Euler(0, 180, 0);
            dretaMov = false;
        }
        else if (this.transform.position.x < player.transform.position.x)
        {
            transform.rotation = Quaternion.Euler(0, 0, 0);
            dretaMov = true;
        }
    }

    public override void agressiu()
    {
        if (attack == null)
            randomAttack();
        if (this.transform.position.x > player.transform.position.x)
        {
            transform.rotation = Quaternion.Euler(0, 180, 0);
            dretaMov = false;
        }
        else if (this.transform.position.x < player.transform.position.x)
        {
            transform.rotation = Quaternion.Euler(0, 0, 0);
            dretaMov = true;
        }
    }

    //////////////////////////////////////////////////////////   Atacs   //////////////////////////////////////////////////////////

    void randomAttack()
    {
        randomAtaque = Random.Range(1, 4);

        switch (randomAtaque)
        {
            case 1:

                attack = StartCoroutine(Melee1());
                break;

            case 2:

                attack = StartCoroutine(Melee2());
                break;

            case 3:

                attack = StartCoroutine(DashAttack());
                break;
        }

    }


    IEnumerator cdAttack()
    {
        if (stateSaved == Estat.AGRESSIU)
        {
            yield return new WaitForSeconds(1.3f);

            attack = null;
            stateEnemic = stateSaved;
            hasAttacked = false;
        }

        if (stateSaved == Estat.PERSEGUIR)
        {
            yield return new WaitForSeconds(0.3f);

            stateEnemic = stateSaved;
            hasAttacked = false;
        }

    }

    IEnumerator Melee1()
    {

        animator.Play("Attack1Rogue");
        audio_source.PlayOneShot(rogueatk);

        yield return new WaitForSeconds(0.8f);

        animator.Play("IdleRogue");

        StartCoroutine(cdAttack());

        hasAttacked = false;
    }

    IEnumerator Melee2()
    {

        animator.Play("Attack2Rogue");
        audio_source.PlayOneShot(rogueatk2);

        yield return new WaitForSeconds(0.8f);

        animator.Play("IdleRogue");

        StartCoroutine(cdAttack());

        hasAttacked = false;
    }

    IEnumerator DashAttack()
    {

        animator.Play("DashAttackRogue");
        audio_source.PlayOneShot(rogueatk3);

        yield return new WaitForSeconds(0.3f);

        if (this.transform.position.x < player.transform.position.x)
        {
            rb.AddForce(new Vector2(20, 0), ForceMode2D.Impulse);

            yield return new WaitForSeconds(0.4f);

            rb.AddForce(new Vector2(-20, 0), ForceMode2D.Impulse);
        }
        else if (this.transform.position.x > player.transform.position.x)
        {
            rb.AddForce(new Vector2(-20, 0), ForceMode2D.Impulse);

            yield return new WaitForSeconds(0.4f);

            rb.AddForce(new Vector2(20, 0), ForceMode2D.Impulse);
        }


        yield return new WaitForSeconds(1f);

        animator.Play("IdleRogue");

        StartCoroutine(cdAttack());

        hasAttacked = false;
    }

    //////////////////////////////////////////////////////////   Altres   //////////////////////////////////////////////////////////

    public override void ResetRogue() 
    {
        this.hp = this.hpMax;
        stateSaved = Estat.PATRULLA;
        ChangeState(Estat.PATRULLA);
        this.transform.position = new Vector2(103, -366);
        this.dretaMov = true;
        Girar();
    }

    public override void Girar()
    {
        if (dretaMov == true)
        {
            transform.rotation = Quaternion.Euler(0, 0, 0);
        }
        if (dretaMov == false)
        {
            transform.rotation = Quaternion.Euler(0, 180, 0);
        }
    }
}
