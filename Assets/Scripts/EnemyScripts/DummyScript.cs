using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DummyScript : MonoBehaviour
{

    [SerializeField] GameEventInteger MostrarPopUp;
    [SerializeField] Animator anim;
    [SerializeField] ScriptableProta scrProta;
    [SerializeField] GameEventInteger eventMostrarDMG;

    public void RecieveDmg(int n, int forceX, int forceY, GameObject posDmg)
    { 
        MostrarPopUp.Raise(n, n, n, new Vector2(this.transform.position.x, this.transform.position.y + 1));
        anim.Play("Dummy_Hit");
        StartCoroutine(GoBackIdle());
    }

    IEnumerator GoBackIdle() 
    {
        yield return new WaitForSeconds(1f);
        anim.Play("Dummy_Idle");
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.gameObject.tag == "PlayerHitbox")
        {

            this.RecieveDmg(scrProta.atk, 2, 2, collision.gameObject);
            Debug.Log("Hiteo normal");
        }
        if (collision.gameObject.tag == "WeaponRed")
        {
            this.RecieveDmg(scrProta.atk, 4, 4, collision.gameObject);
            Debug.Log("Hiteo red");
        }
        if (collision.gameObject.tag == "WeaponBlue")
        {
            this.RecieveDmg(scrProta.atk, 27, 5, collision.gameObject);
            Debug.Log("Hiteo blue");
        }
        if (collision.gameObject.tag == "WeaponYellow")
        {
            this.RecieveDmg(scrProta.atk, 2, 2, collision.gameObject);
            Debug.Log("Hiteo yellow");

        }
    }

}
