using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{

    [SerializeField] AudioSource aud_src;
    [SerializeField] AudioClip buttonMenus;

        
    public void btnMenus()
    {
        aud_src.PlayOneShot(buttonMenus);
    }
}
