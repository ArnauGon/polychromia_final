using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerStartLevel1 : MonoBehaviour
{
    public bool enter, autors, titol, exit;

    [SerializeField] GameObject gameTitle;
    [SerializeField] GameObject gameAutor;
    [SerializeField] GameObject colidermapa;
    [SerializeField] GameEvent activateDesactivatePlayer;

    [SerializeField] GameObject[] llistActivable;
    

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (enter) 
        {
            collision.GetComponent<PlayerScript>().OnCanMove = false;
            collision.GetComponent<Rigidbody2D>().gravityScale = 0.5f;
            collision.GetComponent<Rigidbody2D>().drag = 0.5f;
            for (int i = 0; i < llistActivable.Length; i++)
            {
                llistActivable[i].SetActive(false);
            }

        }
        else if (autors)
        {
            gameTitle.gameObject.SetActive(false);
            gameAutor.gameObject.SetActive(true);
        }
        else if (titol)
        {
            gameTitle.gameObject.SetActive(true);
            gameAutor.gameObject.SetActive(false);
        }
        else if (exit) 
        {
            collision.GetComponent<PlayerScript>().OnCanMove = true;
            collision.GetComponent<Rigidbody2D>().gravityScale = 1f;
            collision.GetComponent<Rigidbody2D>().drag = 0f;
            colidermapa.GetComponent<BoxCollider2D>().isTrigger = false;
            gameTitle.gameObject.SetActive(false);

            for (int i = 0; i < llistActivable.Length; i++)
            {
                llistActivable[i].SetActive(true);
            }
        }
    }
}
